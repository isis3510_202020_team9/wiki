The following section refers to the interview around the problem of self medication. This interview was developed with the help of Maria Fernanda Morris, a young woman from our university that suffers from periodic headaches.

# Interview template
Here is the interview template that was used to interview Maria Fernanda, with her complete conscent for gathering a recording of the interview and obtaining relevant data from it which is going to be used only for academic purposes.
1. *Presentation, introduction of the topic of the interview*  
    * Good evening, this interview that I'm going to develop is around self-medication. To do this, I'll ask you some questions to have more information on the possible problems and issues that are involved in this topic.
2. *Demographics and context specific questions*
    * How old are you?
    * Do you study, work or do other things ?
    * Do you have doctors in your close family?
    * How frequently do you buy medicines?
    * How frequently do you go to the doctor?
3. *Question regarding the person's experiences with the topic*
    * What do you think about self-medication?
4. *Questions to get in depth into the topic's problems*
    * Do you automedicate yourself?
    * Do you feel that self-medication is good or bad?
    * Have you had any issue or situation because of self-medication? Do you think you could have some because of it?
    * Do you get informed before automedicating? How do you do it?
5. *Questions regarding user story and feelings around the topic*
    * When you first automedicate how was that process? what did you do or feel?
    * How was your process to decide self-medication?
6. *Present hypothesis 1 to the user and ask his/her opinion*  
    * "People automedicate based on what they see in media or what they learn from the experience of others but not necessarily knowing with certainty the medical or scientific reason (fact). This means that they do not investigate to get informed appropriately". Do you agree or disagree and why?
7. *Present hypothesis 2 to the user and ask his/her opinion*  
    * "People are afraid to go to the doctor and discover they suffer from a disease". Do you agree or disagree and why?
8. *Questions about other reasons related to the topic*  
    * What other issue do you think exists around self-medication?
9. *Questions about user's solutions to the issues of the topic*  
    * How do you affront the issues that surround self-medication?
10. *Present possible solution and ask for opinion*  
    * If an app has been developed to inform people about medicines that they pretend to consume, and this information is trustworthy would you like it? would you use it? Is it useful to you?
11. *Thank the person who participated on the interview*

# Hypothesis 
The two hypothesis defined for this problem (that were validated in the interview) are:
1. People take their medcation based on what they find on media and what they hear from close friends, but not knowing why they take it in the first place; its an issue of misinformation.
2. People are scared of doctor's appointments and discover a disease in them.

# Interview audio
The following link contains the audio that was recorded when developing the interview:  
[Go to the audio](https://drive.google.com/file/d/1kq17QZ9qxwujiJ8Zy41QHVKKQPOY0LRY/view?usp=sharing)

# Analysis and feedback
The analysis from the interview developed can be summarized in the following points:  

## Particular interview

### Basic information
**Name:** Maria Fernanda Morris  
**Age:** 20

### Interview summary
 - At first, Maria Fernanda answered basic demographic questions that were answered normally. Here, is important to notice that the is desn't have relatives related to medicine. She buy medicines that whe know and has used before, most of them common medicines to solve common illnesses. She goes to the doctor once a year or less.

 - Her perception about self-medication is against it. She only says that this must only be practiced for simple illnesses such as headaches. That last condition is the only thing that she takes into account to choose self-medication.

 - She hasn't had any problem when self-medicating. She **always** searches the medicine she's going to consume before doing it. This research is done through internet. Sometimes she self-medicate with common medicines and does not research about them, just because the experience of her relatives has been good.

 - Her steps are the following: 1. Research on the medicine or get informed about it (experience from other in some cases or research); 2. Buy the medicine; and 3. Consume it.

 - She self-medicates when she feels really bad and knows that it isn't necessary to go to the doctor (because of the long and difficult process).

 - She strongly agrees with the first hypothesis, and also comments that her grandmother is a great example of that hypothesis, because is anold person and she thinks that population of that age tempt to be  misinformed or don't like to be told what they have to do.

 - With the second hypothesis, she thinks that not in all the cases apply, but it is something that is related to her other grandmother.

 - She says that self-medication can lead to wrongly medicating and underestimate dangerous illnesses.

 - She likes the solution proposed and also comments that it can be improved by taking into account the risks of informing people wrongly (They medicate when they maybe have to go to the doctor), and with this, warning them about the possible illnesses they could be suffering. With this, a doctor can be contacted with the solution proposed if the case is extremely dangerous.

## Massive interview
For improving the insights-gathering process, we decided to build a massive form and send it to random people, so our hypotheses could be validated in a better way:  

*Number of participants* --> 29
### Demografic Info
 - The people that filled the form have ages between 20 and 25 years old. The mayority of them have 20 (31%) and 21 (44.8%) years old.  
 ![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/age-selfmedication.png)

 - From the population, 27 out of 29 are currently studying.  
 ![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/labour-selfmedication.png)

 - Almost half of the population (48.3%) have relatives that are doctors.  
 ![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/relative-selfmedication.png)

### About Self-medication
 - The perception around self-medication between the people that filled up the form is almost the same. Almost all the responses **do not agree** with self-medication and think that it is dangerous for people to practice it without any previous knowledge of the medicines they are consuming. A few of them also mentioned that it **may be good** but **only** with medicines that are used for common illnesses.

 - The mayority of the population (82.8%) that filled up de form have self-medicated before.  
 ![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/auto-selfmedication.png)

    * The reasons on this self-medication trend are explained by different reasons. Some people decide to practice it so be relieved from a recurrent illness (common ones) or pain. Others do it because they have used that medicine before and/or it has been recommended by a doctor (sometimes a relative).
 
    ***Insight:*** Even though the mayority of people are against self-medication or think it is not good, most of them practice it.

 - The people that answered the form, investigate and do research when they want to self-medicate through Internet in general (Google, Vademecum websites, ), by asking to a doctor (relative or friend) or simply knowing that it worked before on a similar illness.
 
   ***Insight:*** When self-medicating, most of the people tempt to research on the internet or ask to experts (doctors or relatives) before consuming the medicine.

### Validating hypotheses
 - On the first hypothesis, most of the population agreed with it and just a few had doubts on it.  
 ![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/h1-selfmedication.png)

    In this hypothesis, people thought and agreed that the lack of information was one of the main causes for self-medication. Also, they say that media is a very harmful mean of getting informed when self-medicating and that a very good example is the COVID-19 pandemic's self-medication. They think is more a matter of the colombian culture and the bad health system in the country.
    
    ***Insight:*** Event though the mayority of people research when self-medicating, a high number think that they are more influenced by media and experience from others when deciding to self-medicate.

 - On the second hypothesis, the responses were more uncertain.  
 ![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/h2-selfmedication.png)

    For this hypothesis, people were more diverse in their opinions. Some of them think that the lack of information can produce that fear and encourage self-medication, while others think that there's not a real fear but just preference to avoid the public health system in Colombia and decide to self-medicate.

    ***Insight:***  People do not really fear to go to the doctor in the mayority of cases. Although, there is a very high potential in people to get easily persuaded to self-medicate.

### Possible solution
 - When presenting the possible solution to people, they agreed in most of the cases. They thought that the idea was good but it already exists in different websites. Also, they commented on the risk that the solution implies, if there's misinformation on it or even if there's not a control on what to inform people to consume.

    ***Insight:*** People prefer websites to get informed because they don't  self-medicate frequently. With the correct features this preference could change.

For more detailed information on the form's results, click this [link](https://docs.google.com/spreadsheets/d/1IbfeXuKD5x3XhQfAz34MEHHMrxQd-vLIv7rIFOIxYaE/edit?usp=sharing).

# Situations and Insights
The following situations and insights were derived from the interview. 

## Situations
| # | What? | How? | Why? | Who? |
|:-:|:-:|:-:|:-:|:-:|
| 1 | A person suffers from different affections, mostly common ones as headaches. | He has problems with parts of their body which occasionates pain. He feels really bad. | Because of illnesses or periodic pain, and do not need medication (think he doesn’t need) | A man of 21 years old. |
| 2 | A man does a research on possible medicines for an illness | Through internet, and search sources (i.e. Google) feeling curious about what to find. | He wants to know more about a medicine to consume. | A young man of 24 years old. |
| 3 | A man is suffering from pain. | He feels bad and ill suddenly but does not trust the medicines. | The person don’t like to take medicines to solve the issue. | A young man person of 25 years old. |
| 4 | An old woman doesn’t go to the doctor. | She doesn’t feel comfortable there and doesn't like to go. | She thinks she has more experience with self-medication and trusts more that.  | An old woman of 65 years old. |
| 5 | An old woman doesn't go to the doctor. | She feels afraid and uncomfortable and she haven’t had good experiences before there. | She doesn’t like hospitals nor doctors. | An old woman of 63 years old. |
| 6 | An old woman is afraid of going to the doctor. | She feels more confident in her decisions. | She doesn’t like to wait at the EPS, and deal with the processes of it. | An old woman of 65 years old. |
| 7 | A young person self-medicates because she has relatives that are doctors. | Buys recommended medicines from her nearest drugstore, feeling confident on her relative's advices. | She trusts the information her relative (doctor) provides. | A young woman of 23 years old. |
| 8 | A man buys medicine that he knows that work for a certain illness. | Buying it at the nearest drugstore or supermarket, feeling clever. | Because he has used it before and knows it works. | A young man of 21 years old. |
| 9 | A young woman hears from a friend or relative that a certain medicine works for some sort of illness. | She buys the medicine with confidence to consume it when suffering from the illness. | To prevent the possible pain related to the illness. | A young woman of 23 years old. |
| 10 | A young woman only trusts a medicine if she first researches it and her doctor confirms its reliability. | On the internet and Vademecum sites. Also calling a known doctor, feeling doubtful. | She has to be completely sure about what she is consuming. | A young person of 20 years old. |

## Insights
1. Young people suffer from common illnesses such as headaches, that don't need to be solved necessarily by a doctor.
2. Most of the people research on medicines that they want to consume.
3. Some people prefer not to take medicines and automedicate when they feel bad because of some reason.
4. Old people tempt to trust more on their judgement than on a doctor's point of view. Therefore, they avoid going to the doctor.
5. Some people are afraid to go to the doctor and prefer to self-medicate. This is more common in old people.
6. Old people sometimes feel more confident in their decisions to self-medicate because they don’t like to deal with long and tedious processes at the EPS.
7. Some people prefer the tips from a relative which is related to medicine that going to the EPS.
8. People buy medicines that they know work for a certain illness, it means that they buy drugs that they have used before.
9. Young people trust the experiences from others when deciding to buy medicines for an illness.
10. Some young people are more likely to investigate and validate what a medicine can produce (effects, counter effects, etc.) to be sure to consume it.

