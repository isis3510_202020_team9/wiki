# Interview templates
1. *Presentation, introduce the topic of the interview*
    * Good afternoon Luisa. Today we are contacting you because we are studying the context of micro-enterprises, which they are getting pretty popular and common in this pandemic period, and we want to ask you some questions regarding your daily activity in your own entrepreneurship and your opinion around this idea.
2. *Demographics and context specific questions*
    * How old are you?
    * What is your current role? (Student, Employee, Independent, etc.)
3. *Questions about their daily routine around their micro-enterprise*
    * Regularly in one day, ¿How many orders do you receive approximately, and  how much time do you spend normally working in these orders per day?
4. *In-depth questions about their obstacles and general issues when dealing with this topic*
    * Do you think promoting your micro-enterprise through apps like WhatsApp or Instagram is "not efficient" or something with room of improvement?
    * Have you had any inconvenient, issue or discussion given the fact that you handle your business through these apps mentioned before?
    * Do you know/have contact with any nearby micro-enterprise like you that has experienced these kinds of problems?
5. *Question about the interviewed's feelings when dealing in real time with the discussed problematic*
    * How did you handled a situation when an order goes wrong (regarding issues with the apps mentioned) and a customer gets angry at the service? How did you felt when this occurred?
6. *Ask for the interviewed's opinion regarding Hypothesis 1*
    * "The amount of information posted in Massive WhatsApp groups is overwhelming and disorganized". Do you agree or disagree? Why?
7. *Ask for the interviewed's opinion regarding Hypothesis 2*
    * "I can't easily find the active micro-enterprises near my location that can either help me out or get to compare with my own micro-enterprise (prices, presentation, etc.)". Do you agree or disagree? Why?
8. *Ask to the interviewed about other issues with her micro-enterprise*
    * What other reasons do you think makes these platforms a NON-IDEAL method for your micro-enterprise?
9. *Ask the user how she solves these issues at the moment?*
10. *Present our proposal to the user and ask her opinion about it*
    * What do you think about a standalone platform aiming at micro-enterprises like yours, that can filter results according to your location and other preferences a user have, and moreover going through the current pandemic situation?
11. *Last words and thanks*

# Hypothesis 
The two hypothesis defined for this problem are:
1. The amount of information inside WhatsApp masive groups is overwhelming and disorganized.
2. I cant easily find the micro-enterprises near my location that might interest me, or another micro-enterprise with my same product and find out what I can improve in my own product.

# Interview audio
To access the original version of the interview click in this [link](https://drive.google.com/file/d/1jpklsmXVQvpAEkp8c_Q1fGYe9L5X1FTh/view?usp=sharing).

# Interview Analysis
The overall interviews can be divided in two categories:

## Individual interview
Some of the highlights of the interview are shown here, and a more in-depth analysis is shown in the next point of this document.
- The interview was targeted to Luisa, a 27 year old lady from Bogotá that has spent most of her lifetime with her family to grow a bakery business and had to migrate this activity in the safety of their home. 
- With this pandemic situation, she was one of the cases that used Instagram for publicity and promoting her micro-enterprise, and used WhatsApp for management of individual orders.
- WhatsApp is not good for promoting your product (through massive groups or mailing lists), but is even worse for managing individual orders, the nature of chats is not even close to decent when it comes to diferent baking requests, payments, shipment, etc.
- It is really *really* frustrating to make mistakes in orders or when in general something goes wrong with them, and sadly WhatsApp makes it more easy for this to happen.
- She usually knows about her friends and direct contact's micro-enterprises but not really about the ones all her neighbours or people nearby in general, given how these connections in platforms like Instagram and WhatsApp work.
- When we proposed the idea, she thought it was a great idea and probably something micro-enterprises *need* for the management of orders and promotions in one standalone platform.

## Massive interview
This interview consisted on a short form delivered in entrepreneur groups on Facebook and Whatsapp, and these were the results:

Participants: 23  
Demographic info:  
* There are various ages registered, most trending in the ages between 19 and 22.  
![ep1](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/EmprendePlus1.PNG)  
* Most people (60.9%) were students, and the rest worked either as employee or independent.  
![ep2](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/EmprendePlus2.PNG) 

Opinions:
* When asked for their opinion about promoting their micro-enterprise in WhatsApp or Instagram, more than half of the answers (about 58%) were in favour of this decision, it was considered by them one of the most efficient methods to show their business to the world. In the other hand, the rest of the replies (the other 42%) said that it wasn't enough, that most of the times it was considered spam or innecesary. 
* When asked for their opinion about our first hypothesis, these were the results:  
![ep3](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/EmprendePlus3.PNG)  
***Insight:*** Promoting your service/product in a WhatsApp entrepreneurship group can actually be overwhelming and disorganized, without a doubt.
* When asked for their opinion about our second hypothesis, these were the results:  
![ep4](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/EmprendePlus4.PNG)  
***Insight:*** You don't necessarily look up for a micro-enterprise, instead you casually find out about these through networks like WhatsApp or Instagram, you determine if its a need you have or might have eventually, and keep its reference.
* We asked about their main methods of promoting their micro-enterprise, and long story short, they all replied "Social media, **Instagram**, Facebook or WhatsApp". There were one or two that replied with email chains and referals as aditional, but they also used social media mainly.  
***Insight:*** Any social network, Instagram mainly, is very useful for publishing advertisements of my products, it has great tools and plugins that allow me to decorate my ads in appealing way for the common user.
* When we asked for their opinion about our proposal of a new platform exclusively for them, the overall feeling was that it was a great idea but it has to be well designed and intuitive, given that it's simpler for everyone to go back to basics (social media) instead of sticking to a standalone platform if they dont *really* help out.  
***Insight:*** An ideal platform would apply a new schema for handling inventory and orders, and should be a more robust, simple and straightforward method compared to WhatsApp’s chats management.

For the detailed answers of the form, you can check them in this [link](https://docs.google.com/spreadsheets/d/1SPCWteJG_h7_D5z78YAh-3KwEXxwwcYuUoPxSi3PqGE/edit?usp=sharing).

# Situations and Insights
## Situations
| Sit. | What?                                                                                                                                                     | How?                                                                                                                                                                              | Why?                                                                                                                                                                               | Who?                                                                                                                              |
|:---------:|-----------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
|     1     | Instagram is a great platform for advertising my cakes, muffins and cupcakes                                                                              | The platform has nice templates and color themes that show content attractive for people                                                                                          | Instagram is one of the most used social networks in the country, with the correct strategy you can advertise “anything” there                                                     | The customers that watch Instagram at a constant ratio                                                                            |
|     2     | Is difficult for me to handle customer’s orders correctly in WhatsApp                                                                                     | Every order can be represented as a single chat, and many chats with various requests in each chat/order is tedious to handle.                                                    | Many chats in a single scrollbar, every single one asking different products, it can be complicated to handle by one single person manually                                        | The employee from the micro-enterprise in charge of handling orders                                                               |
|     3     | Customers get worried about their orders and the money used to pay for those, and they get “annoying” in chats                                            | These micro-enterprises usually don't count with certified/official payment methods that ensure safety on transactions                                                            | Most of micro-enterprises are for small amounts of users, not requiring a robust payment method                                                                                    | Customers worried about their money                                                                                               |
|     4     | Luisa doesn’t really know about all nearby “peers” that offer either the same or different products/services.                                             | She doesn’t necessarily have the contact/phone number of all neighbours or close people                                                                                           | There is a missed opportunity to connect with other people if she doesn’t have any connection with these people through any of the platforms mentioned                             | Luisa, interested in knowing about nearby possible alliances                                                                      |
|     5     | Limitations from these platforms can result in an unsatisfied customer, resulting in frustration for her                                                  | The situation with handling many chats concurrently, managed by a human person, can lead to errors (incomplete orders for example)                                                | As mentioned in #3, customers get very worried with their orders in this context, and a mistake or even an entire miss in an order wont make the customer happy at all             | The person handling the requests and the customers itself.                                                                        |
|     6     | Promoting her service/product in massive group chats in WhatsApp is sort of disorganized and unproductive                                                 | A lot of other micro-enterprises like her post their ads in these kinds of groups, and a regular user trying to get all the services posted there will probably skip most of them | These kinds of groups have so much information in its chat that it can be even considered as spam for some people, so is even possible nobody actually reads your ad posted there  | She and the rest of users that are part of massive entrepreneurship groups seeking for services that might need some day          |
|     7     | You don't usually seek for micro-enterprises, you casually find them in social media, local feeds, etc. and you keep them in case you need them some time | Usually you might be looking at Instagram for example and you find a nice ad about cakes near your location, and you keep it for a nearby birthday for your daughter for example. | Customers prefer lower prices and best quality, and micro-enterprises tend to provide exactly this, so usually you keep their information with this goal in mind                   | Customers that usually browse social media and group chats and that can casually find a good service/product at reasonable price. |
|     8     | She constantly seeks for mutual support, not competition at all                                                                                           | She is always open and looking for alliances with others to improve their own product, service or even perception of customers                                                    | Micro-enterprises don't tend to sell massively to destroy their competence, is more to just simply sell to a small niche and earn some extra ingress.                              | Customers in nearby areas, including even micro-enterprises that support other micro-enterprises                                  |
|     9     | A new platform would be helpful if it enables more connections between micro-enterprises and customers/other micro-enterprises                            | With the input of any user location, you can filter all ads and proposals to only nearby micro-enterprises                                                                        | As mentioned in #8, is a great advantage to know about other micro-enterprises for insights (prices of others, possible alliances, reaching more users that doesn’t know me, etc.) | her micro-enterprise and the customers that are physically nearby, like on the same block or neighbourhood                        |
|     10    | An ideal plus of swapping to a new platform is a clean, robust ordering system that fulfills the issues encountered with WhatsApp                         | Maybe with a in-app form that states the order, date and additional comments, and the payment methods of a particular micro-enterprise                                            | Given what is in the situations above, is clearly stated that WhatsApp is not a good app to manage your orders, the nature of chats is not ideal to keep control of your selling.  | Her micro-enterprise and the customers as well                                                                                    |
## Insights

1. Instagram is very useful for publishing advertisements of my products, it has great tools and plugins that allow me to decorate my ads in appealing way for the common user.

2. For the management of each particular order, WhatsApp can be a little messy to use because the fact that you need to handle many chats, each one with completely different order requirements, its very common to lose track of what is going on.

3. Customers are constantly putting pressure about the ir orders, and get pretty pushy with them given that these are popular/unofficial sells, with no certified guarantees of your money in most of the cases, so they get very worried about their shopping process.

4. The interviewed doesn’t really know/have connection with micro-enterprises from neighbours or people in the same location, given that she doesn’t have the phone numbers or tag names from everybody, so there are a lot of possible connections that can’t be done for this issue.

5. The fact that a customer can end up dissatisfied or with a negative reference of the service because of platform limitations make our interviewed very frustrated, given that is not necessarily a mistake from her rather than limitations that get out of hand.

6. Promoting your service/product in a WhatsApp entrepreneurship group can actually be overwhelming and disorganized, without a doubt. 

7. You don't necessarily look up for a micro-enterprise, instead you casually find out about these through networks like WhatsApp or Instagram, you determine if its a need you have or might have eventually, and keep its reference.

8. The idea of micro-enterprises is to help nearby people to make progress with their daily activity, not necessarily compete between each other.

9. A new platform won’t make a difference if it doesn’t promote connections between micro-enterprises with each other and new customers nearby.

10. An ideal platform would apply a new schema for handling inventory and orders, and should be a more robust, simple and straightforward method compared to WhatsApp’s chats management.
