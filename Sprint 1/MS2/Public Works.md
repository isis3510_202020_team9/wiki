# Interview templates
1. *Presentation, introduction of the topic of the interview*
> Good evening, this interview that I’m going to develop is around public works in our city. To do this, I’ll ask you some questions to have more information on the possible problems and issues that are involved in this topic. Any doubt you have, tell me.

2. *Demographics and context specific questions*
    * What is your age?
    * Do you study, work or do other things ?
    * What do you know about the works that are being carried out at the moment in your sector?
3. *Question regarding the person’s experiences with the topic*
    * What perception do you have about the public works in your city?
4. *Questions to get in depth into the topic’s problems*
    * What do you feel when the process of a new public work is not clear?
    * Have you ever made a complaint about a project of this type?
5. *Questions regarding user story and feelings around the topic*
    * When do you suffer from the problem?
    * What would you do step by step to find out information about public works in the sector?
6. *Present hypothesis 1 to the user and ask his/her opinion*
    * Do you agree with the following hypothesis? : "Disinformation regarding public works causes people to have a negative opinion about them."
7. *Present hypothesis 2 to the user and ask his/her opinion*
    * Do you agree with the following hypothesis? : "The inaccuracy of the schedules in which the works are carried out affects my personal life due to the noise"
8. *Questions about other reasons related to the topic*
    * Do you think there is any other reason associated with this problem?
9. *Questions about user’s solutions to the issues of the topic*
    * How would you solve this problem today?
10. *Present possible solution and ask for opinion*
    * If there was a platform that provides you with real-time information on the state of public works in the city, would you like it? Would it be useful? Would you use it?
11. *Thank the person who participated on the interview*

# Hypothesis 
The two hypothesis defined for this problem are:
1. Misinformation about public works make people have as default a negative opinion about them.
2. Inaccuracy in the exact timestamps in which a public work is in progress reflects a bad performance in my job/study/personal activities.

# Interview audio
To access the original version of the interview click in this [link](https://drive.google.com/file/d/1pDnwyrAePDBJerA9JqQt7Vy-QcMgPFTB/view?usp=sharing).

# Analysis and Insights
**Particular interview**

*Basic information*

Name: Fabiola Rey

Age: 53

Working at la *Contraloría General de la República*

1.  In terms of the **public works** that have been done in the recent months she said that **she doesn\'t know anything** about it (like
the estimated time, the schedule, the contractors, etc).

2.  She emphasizes the **payment** that the citizenship must make in order to accomplish their **tax liabilities**.

3.  Most of the public works **don't comply with the date of delivery**.
    In some cases, it is necessary to **pay multiple times** for the
    work because the contractors change( for new mayor elections) or
    the viability studies fail.

4.  At the moment, her company has a *real time and confidential
    platform* that helps with data related to **fiscal control and
    monitoring** the resources of the State. She thinks that this
    information should be open to anyone.
    > More information [here](https://www.contraloria.gov.co/contraloria/la-entidad/organigrama-y-dependencias/direccion-de-informacion-analisis-y-reaccion-inmediata-diari-)

5.  She agrees that **the cause of** **the negative opinions** about the
    public works in the city are related to the **lack of
    information**.

6.  Also she would want to know this type of information in order to
    make decisions about her daily route to the job but she doesn\'t
    think that the **schedule of the public work** affects her
    routine.

7.  She thinks that *if the people could have this information*, the
    impact would be huge because the contractors, the municipality and
    the shift leaders will **feel more pressure about the way they
    work**.

8.  **It is very hard to know what is happening inside**. She thinks
    that the information delivered by the **newspapers** is **not
    enough** because they can not track and control step by step the
    status of each public work in the city.

9.  When we propose the **app** idea she **agrees** with it. She
    believes that with the correct information the people could make
    big changes.

**Massive interview**

Participants: 17

Demographic info:

-   Age: A great part of the participants are less than 22 years old.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/PublicWorks1.PNG)

-   Working/Studying: The 70% are college students.

-   What is their perception about the public works in the city?
    -   Delayed
    -   Deficient
    -   A corrupt practice
    -   Expensive
    -   Annoying for the traffic and noise
-   How much information do you know about public works?

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/PublicWorks2.PNG)

***Insight**: The people don\'t know about what is happening with the
public works.*

-   According to the first hypothesis\...

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/PublicWorks3.PNG)

***Insight***: The results show us that the hypothesis is not
certainly clear for all the participants. However, for most of them
they agree with the idea of thinking that negative opinion related to
this topic is generated by the lack of information.

-   According to the second hypothesis...

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/PublicWorks4.PNG)

***Insight***: The opinion is divided. For the majority of the people
the schedule of the public work affects their personal life (noise).
Nevertheless, some people don\'t think that this could affect them in
any way.

-   How do you get information about this topic?
    -   From my family
    -   Newspaper
    -   Social networks: Twitter, Facebook
    -   Major of Bogotá information channels.

- What do the users think if an application exists and gather all this information?

The participants are divided into two groups. The first group are the
people who think that the app would be useful and great for their
lives and the second group are people who think that it is useful but
they won\'t use it.

For the detailed answers of the form, you can check them in this [link](https://docs.google.com/spreadsheets/d/1cl752t8xL8F89tPTr0d1IlkLDdBjdIz7srgOHTO-Lpw/edit?usp=sharing).

# Situations
| Situation | What?                                                                                             | How?                                                                                               | Why?                                                                                                                                          | Who?                                                               |
|:---------:|---------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------|
|     1     |                  A young man has a bad opinion about the public work in the city.                 |                     He gets angry with their political representatives.                     |                          He thinks that the public works are delayed, deficient and sometimes are a corrupt practice                         |                   A young man ( 21 years old ).                  |
|     2     |                         A woman lose confidence about the public works.                        |                           She disagrees with the taxes policies.                           |                                              The public works take a lot of time to be finished.                                              |                         A woman at her 50s.                        |
|     3     |              A woman lose her money when she pays the taxes liabilities.              |                       When she has to pay the “valorization” of her zone.                      |                           The government argues that the zone will enhance and the citizens can not see the results.                          |                         A woman at her 50s.                        |
|     4     |                      A young man doesn't know anything about the public works.                     | He thinks there is not enough information in the newspapers and social networks to be up to date. |                       He usually doesn't search a lot about it and the newspapers only track the general specifications.                      |                   A young man ( 21 years old ).                  |
|     5     |                                  A woman gets stuck in traffic.                                  |                                She has to recalculate her road.                                |                                               She doesn't know that a public work is on the road                                               |                   A woman at her 50s.                  |
|     6     |           The woman thinks that contractors will feel pressure if the people have information about their job.           |                       The people acquire information about the public works.                       | They will feel pressure because the citizens could know if they are working, how much progress they have,what is the status of the work, etc. |                         A woman at her 50s.                        |
|     7     |                         A woman associates public works with corruption.                        |  When a public work takes more than a certain time(usually years) she thinks in corruption.  |                                      She feels that this type of work has to be done as soon as possible.                                     |                   A woman at her 50s.                  |
|     8     | The people who work at the public entities don't have enough information about the public works.  |      The public employees don't have information when they have to investigate public works.       |                                            There isn't enough open source data about public works.                                            | A woman who works at the CGR(Contraloría General de la República) with 50 years old. |
|     9     |                              A young man cannot work or study at home.                             |       He is trying to work and the noise from the nearby public works makes it difficult.       |                                                    The noise generated by the public works.                                                   |                   A young man( 21 years old )                 |
|     10    |                 A woman gets information about public works at social networks.                 |                         She gets suggested by other users' opinions.                         |                                  She likes to know the opinion from other users about nowadays topics.                                  |                   A woman at her 50s.                  |