# Interview templates
1. *Introduction*

    Hello, I am Daniel and since you are someone currently doing volunteer work I would like to interview you to learn more about volunteering and the process behind it.

    First I would like to ask some demographic questions. How old are you? Are you currently studying or working?

    Now about the topic of volunteering, where have you volunteered?

    Donations are also important, especially during this pandemic. Have you recently made any donation?

    Please tell me more about how you choose the organization in which you would help.

    Finally, I would like to know what motivates you to help.

2. *Problem*

    Have you had any issue or problem while donating?

    Have you had any issue or problem during the process of selecting an organization?.

    Have you felt frustrated during the process of helping?

    Do you know of other people that feel the same?

    Do you know people that wanted to help but haven't done it yet?

3. *Journey*

    Please tell me the step by step process of applying to an organization

    How do you feel about that process?

    Which steps are the most tedious?

4. *Hypotheses*

    Now I will present to you a series of hypotheses and I would like your opinion on those topics:

    -   H1: People can't help because they don't know the people or organizations that really need help.

    -   H2: There aren't any known resources to know how to contribute to society as a volunteer or by donating. 

    -   H3: The current situation has a negative impact on the volunteering and donations work.

5. *Possible solution*

    What do you think we can do to motivate people to donate and volunteer?

    What do you think we can do to accelerate the process of looking for organizations?

6. *Conclusion*

    Thank you for your time. 

# Hypothesis 
The two hypothesis defined for this problem are:
1. People can't support charities because they don't have direct knowledge of people or organizations in charge of them.
2. There are no popular platforms that tell/help people how to contribute as a volunteer or donor.
3. The pandemic, in general, has had a negative impact in my motivation to help charities.

# Interview audio
The following link contains the audio that was recorded when developing the interview:  
[Go to the audio](https://drive.google.com/file/d/1ASnsr38O4CfCu4hyyXu-cmTJhdM5zpuD/view?usp=sharing)

# Analysis and feedback
The analysis from the interview developed can be summarized in the following points:  
## Particular interview
*Basic information*

Name: Laura Ávila

Age: 22 años

Studying at Universidad de los Andes
Volunteer in ' *Con las manos* ' 

1.  In terms of the **donations** that have been done in the recent months she said that she has donated to many people that asked for help during the pandemics.

2.  She considers that people who don't donate are like this because of their way of being and cann't be chaged..

3.  'Con las manos isn't the only organization in where she has helped. She also volunteered in 'Proyecto Fenicia'

4.  She choose those organizations primarily because of rhe location an because these organizationes were focused in teaching kids, something that she is passionate about.

5.  She thinks that the most frustrating part of the process is when she is looking for an organization an they aren't accepting any applications.

6.  She had an issue while donating clothing. The person that was supposed to receive the clothing didn't show up.

7.  She thinks that *if the people could have this information*, there would be more people motivated to donate and help.

8.  The webpages she used in the process of looking for an organization were:

    [HandsOn](https://handson.org.co/)

    [HacesFalta.org](https://www.hacesfalta.org/oportunidades/buscador/?bsClave=Bogot%C3%A1+&bsCategoria=&bsPais=&bsProvincia=)

9.  When we propose the **app** idea she **agrees** with it. She
    believes that with organized information people could choose more easily a volunteering organization.


## Massive interview
For improving the insights-gathering process, we decided to build a massive form and send it to random people, so our hypotheses could be validated in a better way, here are the results:  

Total Answers: 18 

This was the age distribution of the people that were interviewed:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/age-Volunteer.PNG)

Many of the interviewed were young people that where studying at University.

In order to get to a higher audience, the people that we interviewed weren´t only people who had volunteered. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/volunteer-Volunteer.PNG)

We also asked if they donated to causes due to the pandemic.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/donations-Volunteer.PNG)

Also, we asked for how motivated they were to donate and help during the pandemic.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/motivation-Volunteer.PNG)

We can see that most people have donated, an thus have the motivation to help others affected.

Finally, we asked our interviewees to validate our hypotheses and give us feedback about them.

* Hyphotesis 1: " People do not help because they do not know which are the ones that really need help"  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/h1-Volunteer.PNG)

* Hyphotesis 2: " There isn't any known platform dedicated to know how to contribute as a volunteer or make donations"  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/h2-Volunteer.PNG)

* Hyphotesis 3: " he current pandemic situation has affected negatively my motivation to help and donate"  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS2/img/h3-Volunteer.PNG)

For the detailed answers of the form, you can check them in this [link](https://docs.google.com/spreadsheets/d/1JybYiZpXZy6ivXtDkt3qBqzmKFOmfAO5cV9D7Y3_2eE/edit?usp=sharing).

# Situations and Insights
| Sit. | What?                                                                                                                                                                                                                  | How?                                                                                                                                                                                         | Why?                                                                                                                            | Who?                                                                                                                                                                                              | Insight                                                                                                                                   |
|------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| 1    | Laura sees 2 posts on Social media: One that is asking for money donations to help a family during the pandemic. The other post is asking for clothes for children. Laura decides to donate to the first post. | Laura looks concerned and is trying to help. Laura is exhausted from being at home, so she doesn’t want to put much effort in the donation.                                        | Donating clothes implies selecting the clothes and then taking those clothes to the donee. Donating money is easier and faster. | Laura is a student at an University. Her family has enough income to go through the pandemic crisis. She is willing to help others that do not have the same privileges as her.                     | It is more difficult to donate physical materials (Clothing/food) than money.                                                             |
| 2    |   Laura had to travel and missed the first week of her volunteering job. Since the first two weeks are the most important, she had to turn down that opportunity.                                                                                                         |           She was frustrated because they weren't flexible with the dates               |       The volunteering organization had problems in its communication with Laura.                |                         Laura is a student that has a decent income and ocasionally travels.                                                      |      The first volunteering weeks are essential to learn the work                                      |
| 3    | Laura tries to donate clothing but when she tried to deliver the clothes, she had to wait a lot                                                                                                                     | Laura is frustrated because she is wasting time while waiting for the donation to be picked.                                                                                            | There is a lack of communication between the donor and the donee.                                                               | Laura is a woman that is trying to help and willingly goes to the location that is receiving the donations. As much as she is eager to donate, she also values her time.                            | There can be problems during the delivery of the donations.                                                                               |
| 4    | Laura to apply to volunteer at an organization. Because of that, she fills the application form, then hse is interviewed and then she starts her involvement at the organization.                                | Laura is exhausted from the long process that she had to overcome in order to be a volunteer. Eventually, she is happy that she is helping people and making a difference to the world.   | The process of applying to an organization involves many steps to assure the selection of the candidates.                       | Laura is a volunteer that wants to help an organization and has the time to do so.                                                                                                                   | The process of looking for a non profit organization can be long and exhausting: It requires forms, interviews, admission deadlines, etc. |
| 5    | Laura wants an organization in which to volunteer. She looks in different webpages to know more about the possible organizations.                                                                                   | Laura is curious and she wants to look for an organization that matches her passions. She is a little overwhelmed by all of the different organizations that are there.                | The information is disorganized and sometimes it is incomplete. The supply is high.                                             | Laura is a student at University that wants to help but doesn’t know where to begin so she tries to look for organizations by herself.                                        | There are existing web pages with a catalog for the organizations but they are disorganized.                                              |
| 6    | Laura is scrolling in social media when a post about making a donation arises. She then proceeds to make a donation.                                                                                                 | This person was minding her own business before seeing the post. After being emotionally moved by the post, she tries to help by making a small donation.                              | Social Media can be a tool to post information about donations.                                                                 | Laura is willing to donate when there is a chance.                                                                                                                                     | For donations, people don’t look out to donate, instead they donate when an opportunity is presented.                                     |
| 7    | Laura decides not to volunteer because the organization location is too far from where he lives. Instead, he decides to volunteer at his University.                                                                | Laura feels disappointed because that organization really matches her ideals and he wanted to help there but she feels that she is wasting time commuting to that far place.             | Laura was uninformed about the location when he applied. Now during the pandemic, that becomes less of a problem.          | Laura studies at a University and therefore had to commute frequently from his house to the University. However, having to go to another place to volunteer seemed like a lot of work. | Location is a huge factor in,which organizations we choose to volunteer.                                                                  |
| 8    | Laura chooses an organization to volunteer because she heard of that organization from friends or family.                                                                                                           | Laura feels excited and more confident about her decision because now she knows the experience of others.                                                                              | People’s opinions and experiences can guide the person in the process of selecting a volunteering organization.                 | Laura is a woman that takes into account the opinion of others when choosing places to volunteer.                                                                                                    | Friends and family experiences and suggestions have an important role to decide the organization.                                         |
| 9    | Laura wanted to volunteer at an organization but she couldn’t because the deadlines to apply had already passed.  For that reason, she decides to apply elsewhere.                                                                                                   | Laura feels disappointed because that organization really matches her ideals and she wanted to help but now she would have to wait until the applications are open. .                             | The deadlines weren’t clear.                                                                                                    | Laura is a person that is motivated to help others and has a lot of free time during the pandemic, so now she can volunteer in something.                                                             | In order to apply, you need to know the dates in which you can apply.                                                                     |
| 10   | Laura watches news about the current pandemic and is motivated to donate and help in some way.                                                                                                                       | This person is concerned by the current situation in our world and would like to help change this situation for good.  | Laura is motivated to help pecause of her privileged position and because she wants to reduce inequality. | Se is a man in his 20s that is very busy and in his free time he likes to scroll on Social Media and watch news and Tv.                                                                           | The current pandemic is a motivator for people to donate/help                                                                             |

