# Brainstorming advance :earth_americas:

For the ideation step of design thinking, we used the Divergence-Convergence Methodology of brainstorming and came up with various ideas to solve the problem of **Volunteering**. 

The structure of this brainstorm is defined as the main/starting ideas in the squares and the subsequent complements in circles. The whole ideation mural can be found in this [link](https://app.mural.co/invitation/mural/moviles6247/1598134057554?sender=u0569ebba38d5a7f68a3f5515&key=57683b8a-9023-4a2a-aec6-e187410b1b21).


For the Divergence part of the Brainstorming, we used a tip called 'Yes, and', in which we complemented each other's ideas instead of saying 'but...'. After having all the columns with the ideas and their complements we had a voting session for defining the most viable paths to develop. We assigned 3 votes each and selected the ideas that had the most number of votes. These are the solutions that were elected, with the ones with 4 votes the best selected and the ones with 3 votes the next priority ideas. We decided to choose the first two ideas and merge them into one idea because we think that donating and helping in an organization are very related. We also selected the ideas with 3 votes as possible additions to our final product, mainly because of the format and interactions that they enable. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/B1V.PNG)

For the Convergence part of the Brainstorming, we grouped the 4 prioritized ideas with their complementing ideas, and grouped the rest of the ideas, tagged them as **Selected** and **Archived for the future**. From this, we graphed all of the possible 'features' of our solution based on two variables: Importance and Feasibility. With this graph, we could get a grasp of the idea prioritization and what features to implement first. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/B2V.PNG)

Also, we decided to include some smart features so that our solution could be integral and have an impact on the user. We first brainstormed some of the smart features and then we filtered them based on the scope we selected and the other features that we included. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/B4V.PNG)


From all those smart features, we decided to include the following ones:

* The use of a recommendation system to suggest organizations: We selected this smart feature because this feature is essential to achieve accurate recommendations. Also, considering the time constraints and our personal preferences, we think that feature is our 'darling.

* The app could be location-based: We selected this feature since location is a huge factor to decide which organization to volunteer. Also, with this feature, we can explore the internal capabilities of mobile devices.

We didn't select the other tentative smart features because we thought that those wouldn't have a huge impact on the user and were too farfetched.

# Empathy maps :map:

### Empathy Map 1: 
For the first empathy map, we made an empathy map focused on the person that we interviewed first: Laura Ávila. She is currently a volunteer in 'Con las manos' organization and has told us about the whole process of looking for a volunteering organization and applying to one. The link for that empathy map can be found [*here*](https://app.mural.co/invitation/mural/moviles6247/1598283643600?sender=u0569ebba38d5a7f68a3f5515&key=a6663901-24c9-4347-8af6-8ef5133cd18b)

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/EM1V.PNG)

### Empathy Map 2: 

For the second empathy map, we focused on an user that asked for donations on the Facebook groups 'Cbus que deberían meter' and 'Cbus sin censura'. We selected that user because we think that donating is an essential part of helping people and has a high correlation with volunteering and social work. The link for this empathy map can be found [*here*](https://app.mural.co/invitation/mural/moviles6247/1598283670782?sender=u0569ebba38d5a7f68a3f5515&key=b97b62d2-7aec-4899-a40d-45a683395321)


![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/EM2V.png)

### Empathy Map 3
For the third empathy map, we focused on the other side of the volunteering process. Inside the organization, there are people responsible for doing the application process and in charge of the activities in the organization. We tried to connect the pain points that happened in one extreme of the process with the other. The link for this empathy map can be found [*here*](https://app.mural.co/invitation/mural/moviles6247/1598283694485?sender=u0569ebba38d5a7f68a3f5515&key=bca8a455-14c9-44a5-8a6f-414bf58e0f73)

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/EM3V.png)

# Personas :woman: 
For the first persona, we decided to cluster the potential users that are currently Volunteering in an organization and had looked for organizations in the past. This kind of user is the one that knows more about the volunteering process and has the most experience. The format of the persona can be accessed [*here*](https://drive.google.com/file/d/1HU3Yi6UhRnf1CKypQVmzxsCvLyI1FcAf/view?usp=sharing)

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Persona_Volunteering.png)


# PAS
This is the PAS (Problem, Alternatives, and Solutions) for our idea of volunteering. We decided to divide it into three parts. The first one of these three parts refers to the problem and the decision process for it. Then, we describe some of the alternatives of the users and finally the solution that we propose to provide value to the user and solve the problems the user's facing.
## Problem description and decision process :thinking:
When we first get into this problem, the person we interviewed was very related to volunteering and she had been searching for a volunteering organization until she found the correct one for her, and she got into it. Throughout this process, we decided to write her experience in an empathy map that gave us more structured information on her process, routines, and possible issues she faced during her process.

We also realized that she was also related to the problem of donations, so we decided to join this to problems into one. This was mainly because people that are related to volunteering organizations, also are related to donations inside or outside those organizations.

We found that the following points on the process could be rough for her and can have a better approach to improve them:

1. People have to do a lot of research to find their preferred volunteering organization, leading to time wasted that they could invest in other activities.
2. The information of the volunteering organizations is not always right or outdated, sometimes the deadlines for applying to these organizations are wrong on their websites.
3. The volunteering information is spread throughout different sites.
4. It is difficult for people to keep up with the process when they apply to a volunteering organization.


Related to donations, are the following:

1. There are several posts related to donations spread through the internet and especially in social media.
2. People have to read long, emotional-based descriptions on different posts to analyze if they want to donate.
3. The donations are based on crowdfunding on different platforms or banking accounts.
4. Donating things other than money is tedious and it is challenging to see the impact of the cash donations.


To sum up, the problem that englobes all the issues can be written as follows:

The process to find a volunteering organization is tedious, from the time-consuming searching process to the application process. This produces that people are not always entering the volunteering organizations that match with their profiles, experience, and preferences. The donations are spread through different social media and do not have a centralized way to be managed.

## Alternatives :repeat:
For the problem, the users focus on using the Internet and social media as their main alternative to all the issues above, so the list goes as follows:

1. The users search for volunteering organizations through the internet (search engines) and social media, an activity that requires a lot of time because the process is completely manual and is the user's responsibility. This is also used for searching for donations.
2. The users use messaging platforms (Messenger, WhatsApp, Email, etc) and applications to communicate and keep up with the process when they apply to volunteer organizations. This is also used to communicate in the donation process.
3. Banking accounts and platforms of crowdfunding such as Vaki are used to collect money from donations, being difficult to donate with goods instead of money.

## Our solutions :bulb:
The solutions we propose are condensed on the following proposal:

A centralized platform (mobile application) where volunteering organizations can hire volunteers. This platform is powered with a recommendation system to match the user profile with the best possible volunteering organization, based on the user's preferences and current location. With these smart features, the users can select the organization that is best suited for them. The system will also include a donation platform where people can donate money or any other good (clothes, toys, etc) depending on the organization's purpose and needs. The donations are also powered with the recommendation system.
The volunteering process is going to be managed entirely in the platform, so the user is going to be able to see in which stage of the process they are located and see their progress. This allows the user to understand better the process and be prepared for each of the stages.  This platform also encourages volunteering through points that can be earned by the user while he attends to volunteering. These points can be redeemed for different prizes, which also serve as an additional motivator to continue helping and donating.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/B3V.PNG)

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/B5V.PNG)