# Brainstorming advance :earth_americas:

For the ideation step of design thinking, we used the Divergence-Convergence Methodology of brainstorming and came up with various ideas to solve the problem of **Ventures and microenterprises**. 

The structure of this brainstorm is defined as the main/starting ideas in the squares and the subsequent complements in circles. The whole ideation mural can be found in this 


For the Divergence part of the Brainstorming, we used a tip called 'Yes, and', in which we complemented each other's ideas instead of saying 'but...'. After having all the columns with the ideas and their complements we had a voting session for defining the most viable paths to develop. 
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Divergence%20process%20EM.png)

For the Convergence part of the Brainstorming, we grouped the 4 prioritized ideas with their complementing ideas, and grouped the rests of the ideas, tagged them as **Selected** and **Archived for the future**. Inside each group, the idea was to sort every item in the layout from left to right with "most valuable" on the left. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Idea%20prioritization%20ME.png)


These are the solutions that were chosen, with the ones with 4 votes the best selected and the ones with 3 votes the next priority ideas. We decided to choose the first two ideas and merge them into one idea because we think that donating and helping in an organization are very related. We also selected the ideas with 3 votes as possible additions to our final product, mainly because of the format and interactions that they enable

Also, the smart features can be seen at the following image:  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Possible%20smart%20features%20ME.png)

# Empathy maps :map:

### Empathy Map 1: [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598665720542/6e698d175af77ef1ca7f38f33d0bd825819a0cd7)
This empathy map is related to a seeking customer. The main insights from this are the necessity of acquire those type of products to help the ventures to grow.  
If you want to hear the interview: [_Laura interview_](https://drive.google.com/file/d/1uV_DCDH31_5ODocVS3yYuiSrY55JBRGl/view?usp=sharing)
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Ventures%20EM-%20Customer.png)
### Empathy Map 2: [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles4123/1598444588567/c6f4a31900f228ac41c0df6f83fa52f409ac48e1)
This empathy map is related to a casual customer. The main insights from this is that users are constantly hitted with these kinds of businesses every day throw social media, and most of the time is something interesting to acquire/hire.
The information of the interview was only processed for the empathy map, the interviewed person didn't allowed us to store the literal information she provided.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Ventures%20EM%20-%20Customer%20Old%20Woman.png)
### Empathy Map 3: [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598283849958/88809e2656d4ce4a3773850c7c887ce2864d1a83) 
This empathy map is related to the second user, the micro-enterprise members, in this case that provides a product. The interviewed is a 27 year old college student that is baking cakes as her micro-enterprise during this quarantine.
You can see the entire study of this case here, at the individual interview section: [_Luisa's interview_](https://gitlab.com/isis3510_202020_team9/wiki/-/blob/master/Sprint%201/MS2/Emprende%20Plus.md)
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Ventures%20EM%20-%20Microenterprise.png)
### Empathy Map 4: [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598283866656/0fad7509066243f8e93706ab790982f6a162e9d4)
This empathy map is based on a micro-enterprise member that provides a service. The interviewed is a 21 year old college student that does coding tutoring in his free time since his second semester.
If you want to hear the interview: [_Carlos interview_](https://drive.google.com/file/d/1_0xWF6k3IpB7rHAD22ywgnVoMLKvSATD/view?usp=sharing)
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Ventures%20EM%20-%20Microenterprise%20(services).png)

# Personas :man: 
At this point, it was important to define the similarities and differences that existed between the user segments to find the XXXX. Therefore, we realized that each interview represented a way to face the problem and to see it too. While young people viewed entrepreneurship as a source of extra income, older people regard it as their livelihood. For this exercise, we wanted to focus on the first case since we got most of the information from them in the interviews and because we found patterns from them.
Let's see the diagram:
[Persona Link](https://drive.google.com/file/d/1NDRXe5fGvPyvwPHeWdkflIkF8vjO2eBy/view?usp=sharing)

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/PersonaMicroEnterprises.png)


# PAS
In the following sections, you will see our way to build and understand the problem and the solution.

## Problem description and decision process :thinking:

For this particular problem, we seek to be able to characterize who the affected users are and how it affects them. This process occurred organically thanks to the interviews we conducted. Those interviews were generated during the brainstorming process because it was crucial to make hypotheses and contrast with real affected users. Also, the interviews allow us to refine or reject those assumptions to build empathy maps that seriously empathize and not just portray team positions.
In particular, these interviews allowed us to identify various types of users and various ways in which they are affected and solved by the problem in their day-to-day life.
On the one hand, we have the testimonies of two entrepreneurs. Despite undertaking and feeling identified with the problems that we will discuss below, their approach is different.  
The first of them is an entrepreneur who sells biscuits, cakes, and sweets. For her, how she sells and how many people she sells her assets to is the most important thing. She believes that the problem is that the current platforms on which an order is created and completed are not the right ones. From receiving many messages and not being able to process them instantly to the fact of not being able to reach more clients due to the limitations they imply [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598283849958/88809e2656d4ce4a3773850c7c887ce2864d1a83)  
The other case is an entrepreneur focused on service: tutorials for programmers. Although it is a service, there are similarities with the previous testimony. She also believes that these means are not the best to reach people. However, your concerns are more about the way you provide your service and not the chain of events that happen before it. [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598283866656/0fad7509066243f8e93706ab790982f6a162e9d4)  
In this way, upon discovering various ventures, our approach gradually focused on the first type of user. The one who is more interested in the contact, order, delivery, and payment process.

However, the entrepreneurial person is not our only one involved in the problem. It is also important to refer to the user who wants the products or services of the companies: the consumer.
In this user, the consensus is usually easier. Most buyers perceive how the purchasing processes are very slow. Although it is not only about the sale but about the communication with the company, sometimes they take days to respond. On the other hand, consumers perceive that there is no concrete way to trust companies. They do not have ratings so the only way to learn a little more is from comments on their social networks or recommendations from family and friends.

[Empathy Map 1](https://app.mural.co/t/moviles6247/m/moviles4123/1598444588567/c6f4a31900f228ac41c0df6f83fa52f409ac48e1)  
[Empathy Map 2](https://app.mural.co/t/moviles6247/m/moviles6247/1598665720542/6e698d175af77ef1ca7f38f33d0bd825819a0cd7)


Finally, we can list the problems:
* There are no **personalized** channels where you can make purchases from micro-businesses.
* The *communication and response* of companies of the customer requests are usually **late**. 
* **Accumulation of messages** (orders) that avoid being able to respond to requests in time.
* The fact of not having clear payment methods transmits insecurity in some users.
## Alternatives :repeat:
The current alternatives related to the problem are themselves part of it. Users usually communicate through social networks such as Instagram, Facebook, or WhatsApp. Such communication is ideal since these social networks are massively used. On the other hand, the purchase process is usually diverse, from bank consignments to cash on delivery. However, there isn't generic mechanism but the decision is up to the company.  
On the part of the delivery process, the users in question say that the company handles it without the need for external providers. In this way, shipments are not usually made the same day of the order 

## Our solutions :bulb:

We started the process to find the correct solution for our users with a brainstorming session where we sought to bring together those functionalities that would add greater value to users and make the experience unique. [Brainstorming mural](https://app.mural.co/t/moviles6247/m/moviles6247/1598134057554/913ce73f35af6f671b4d7ceefe0f7af9d86b3cc4)


Likewise, to follow the concepts of the class, we begin to imagine that intelligent functionalities could provide us with that differentiation from other platforms or marketplaces. In this way, by talking to the TAs and the teacher, we were able to relate a smart feature to the needs of the users. Hence, we concluded that a chatbot that frees the load for entrepreneurs and makes the process more efficient for buyers was the ideal solution.

Another smart feature we might include is the option to detect nearby micro-enterprises with location services on a specific radius of each phone, allowing the app to filter feed with the nearest products/services for the user.

And last but not least, we want to include an option to detect and analyze a picture of the billing receipt with the phone camera, using this as a certification layer for the billing process.

With these features in mind and the overall analysis of the problematic, our proposal is a mobile application that allows private micro-entrepreneurs to advertise their product/service and manage orders in an organized way. This occurs through a marketplace in which customers can access available products and a chatbot that incorporates multiple payment methods. Likewise, clients will be able to categorize and find ventures related to their interests.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Selected%20Smart%20Features%20ME.PNG)