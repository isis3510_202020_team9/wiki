# New Personas :man:
Here we did **two** aditional personas and an extra one, which refers to the analytics persona. Also is important to remind that at the past microsprint we did one persona([First Persona Link](https://drive.google.com/file/d/1NDRXe5fGvPyvwPHeWdkflIkF8vjO2eBy/view?usp=sharing))

In the first one, we are talking about a prospect persona that would use our solution. This persona needs the solution in order to sell more products and have the best rated cakes.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/PersonaMicroEnterprises.png)

In the second one,([Persona Link](https://drive.google.com/file/d/1txF5mltuetPldfOFhlKr5xGWre6x53n8/view?usp=sharing)) we have also a prospect user but not the one who wants to sell products. This persona wants to find the best products at the lower possible price. Also, she desired to help other entreperneurs to grow and improve.  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/PersonaMicroEnterprises2.0.png)

In the third one,([Persona Link](https://drive.google.com/file/d/1GI6RYpsTN4LJ4gUCcDKeE2ctL6WC-SdQ/view?usp=sharing)) we have to talk about an enterperneur. This type of user center his objectives in the income and a good customer review.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/PersonaMicroEnterprises3.png)

The last one, ([Persona Link](https://drive.google.com/file/d/1TIgyXTZvolcBrm77G7PWS9jyME7h4gPu/view?usp=sharing)) which is the analytics one, is that type of user that wants to improve his business based on data. He know that the information is the most important thing in the world.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/Analytic%20persona%20MicroEnterprises.png)


# PAS :nerd_face:
Here, we transformed the PAS that we developed for the MS3 into a table, which is the following:

| # | Problem | Alternative | Solution |
|:-:|:-:|:-:|:-:|
| 1 | It takes a lot of time to reply to every customer that asks for an order in a WhatsApp chat. | Reply them as fast as possible, despite having a lot of chats constantly active. Also, we might be working in another order at the time, and it needs to be interrupted to reply them | A new chat methodology that involves a bot to reply to new users automatically, and take the orders as well. This saves a lot of time replying to each user. |
| 2 | There is a need for promoting microenterprises,or there won't be any clients that buy from us and support our cause. | Rely on Instagram free ads and joining WhatsApp entrepreneur groups to promote my business, but these are a little disorganized and doesn’t ensure reaching enough people and that these people will read these posts anyway | An independent platform with its own particular feed which is exclusive for these kinds of posts, and I can post knowing it won't be lost in a chat like those WhatsApp groups mentioned, but is correctly stored in the app feed. This gives a special space for these promotions to be shown. |
| 3 | We need to know what are the micro-enterprises nearby my location so I know what are my competition, and more than that, what are my possible alliances  | Having the contacts of some neighbours on our phones and Facebook help out, and this provides connections on WhatsApp and Instagram allowing very limited knowledge about their activity on their micro-enterprises | With a platform designed for micro-enterprises, there is no limit in knowing about only the people we have on our contacts, but to any user that has the app nearby our location, and this feed will show filtered posts as needed.  |
| 4 | We need to receive the payment from our users with more methods other than just cash, something easy, fast and reliable | Currently the most used methods are bank transfers or virtual pockets such as Nequi or RappiPay | Including robust and generic payment methods such as PSE will be a huge plus because we don't limit the user with specific banks or virtual pockets. This enables more payment methods available for customers. |
| 5 | There is a need of knowing what is the opinion of our customers to find improvement points in our products/services | We rely on thanking messages of the customers that ordered anything and comments on ads posted in social media to get an idea of people’s perception. | A rate system in a standalone platform that allows customers to easily give 1-5 starts to a micro-enterprise that serves an order to them. This will result in direct feedback based on a numeric system. |
| 6 | When there are “peculiarities” in one particular order, it is difficult to attend to this when there are a lot of other orders going on in other chats. | Place policy like “After an order is set, there is no way to change it”. Alternatively, we can constantly check WhatsApp (consumes a lot of time) waiting to encounter a case like this | In the chatbot proposed in idea #1, add an option as “ask directly” when special cases like this occur, so that when clicked is notified to us and we can directly attend the customer. This allows customers to make their requests with a human directly. |
| 7 | When providing a one-time kind of service, in most cases customers lose track of the micro-enterprise and we don’t get to know about them or reach them anymore | Adding customers to a mailing list or ask them to like our page in Facebook or Instagram | Add a feature such as “follow site” so that users always get their posts, and propose these after they rated the service they paid for. This allows options for more consistent connections between micro-enterprises and customers. |
| 8 | As a customer, is sometimes difficult to find what you actually need, given that many micro-enterprises post in massive groups or feeds, and is not easy to find what you are actually looking for | Saving the posts at the moment you see them and store them with a label on a notepad or sticky | Adding a label system that tags each post with a specific category, making the filtering process easier for customers. This allows in-app to categorize posts for the customer. |
| 9 | We don’t know a lot of our customers, usually is just communication for an order with a random number on WhatsApp | We just ask for the name and billing information to deliver the product or reach for the service. | With profiles in our platform, we might have a better idea of who our customers are, given the closer communication. |
| 10 | We want to constantly improve the user experience with all the process (ordering, paying, delivering, rating), and is difficult if many people have different perceptions and don’t communicate them | Read the negative comments or constructive criticism in posts we made in social media | Add an option to send textual comments from the user in such an appealing way that is difficult to just skip that step. This gives the option of more detailed feedback for each micro-enter |

# Context Canvas :scroll:
Here is the context canvas for our solution:  
[Context canvas link](https://drive.google.com/file/d/18j3FghtpLE9V8J8lhzPIBr379bhdb6Ad/view?usp=sharing)
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/Context%20Canvas%20Microenterprises.png)

We made this canvas based on the personas we developed at the beginning and the new personas we did. This context canvas groups everything that we have done up to this moment.

# Business Questions :thinking:
[Document link](https://docs.google.com/document/d/1RhlMFhXXHXKvlGbRF1j5PKyobKw_EH49C-7jzGjUxRI/edit?usp=sharing)  
For this process, we hold ideation and classification sessions. In the first place, we can see the categories referenced with colors as follows:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/Categorization-Business%20questions.png)


![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/microenterprises-categ.png)

The questions are listed below:  
1. What is the Android distribution of our client devices? What is the percentage of users that use our platform on an iOS device?
2. What is the average RAM used by the app each minute?
3. Does the app load in less than 4 seconds?
4. What is the monthly amount of crashes for each screen?
5. What is the probability that a user with a phone from 2017 or less has crashes ?
6. How many days have passed since the client last used the app?
7. How much time on average does the user spend browsing the catalog of microenterprises?
8. How long does it take to order a product, in average?
9. Does the user have to leave the app to use any of the payment methods? How frequently?
10. What is the average time to deliver the client a product, and how much is the average cost of this delivery? 
11. What is the distribution of preferred categories per location?Will there be separate panels for different categories?
12. How frequently do the vendors post images of their products in the app? 
13. How many products/services is each vendor offering?
14. What is the percentage of each channel that is presented in the app to communicate with the client? (Whatsapp, phone, Instagram, chatbot)How can a user communicate faster and easier with a micro-enterprise?
15. How can you share an app post through external social media? Which social media sharing is the most used?
16. How much percentage of users is the minimum for distance filtering to be a relevant feature for them? How much percentage of users would be if we include user’s reviews organization instead?
17. What's the percentage of users that use the in-app payment, and what's the other percentage that are paying separately?
18. What percentage of orders couldn’t be resolved by the chatbot?
19. What percentage of users use the camera scanner for their receipts?
20. What percentage of vendors are looking at the analytics insights?
21. Do we have some information about user preferred product categories? Which are those?
22. Do we have information about users' average monthly expenses? What is that average?
23. Do we have information about the time that an order takes to be delivered? What’s that time on average?
24. Do we have information about the user comments on their orders? → Yes, we can process that text to user feelings.
25. Do we have information about the users perception about the ventures with a quantitative rate?
26. Do we have information about the proportion (by gender) of users that buy products in the app?
27. Do we have information about the age of the users in order to classify and relate their favorite products?
28. Do we have information about the historic orders? Yes, we could analyze the best days/hours for producing.
29. What is the percentage of entrepreneurships that gain enough to pay taxed to the government? → Sell the data to the DIAN
30. Do we have information about the location of the users that frequently buy products through the app? → We can sell this to the delivery apps like Rappi, to send more people to that area
31. How many days passed since an order greater than 50mil pesos was charged?  
32. What percentage of Vendors haven’t sold anything on our platform?
33. What percentage of orders get cancelled daily?	
34. What percentage of users have bought at least once in the app?
35. Our revenue comes from orders, could we make cobranding to introduce promotions?What percentage of users are selling more than one thing in a product (combos)?
36. What is the price distribution of the products per category? → Could be useful to implement new functionality
37. Would including sales of products outside the app make the user more interested in buying? How frequently do users search for a product that isn’t available?
38. How can the app handle refunds in case of not successful orders? How frequently does this happen? 
39. Is the app fluent in all its  screens (less than 0.2 ms between transitions)?
40. Are the last release bugs lower than our goal (max. 3 bugs) ?


# VD Map :clipboard:
This is the VD Map developed, based on each one of the 40 questions. We decide to classify the questions in possible databases and the process.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/classification-bq-me.png)

Here, we have the VD map of those grouped questions ([VD Map](https://drive.google.com/file/d/1nbPl7lsReBEV7fMv3cQMedg5RqAtnAVD/view?usp=sharing)):

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VD%20Map%20Microenterprises.png)

# Business Model :chart_with_upwards_trend:
This is the lean canvas developed to represent the business model:

## Value proposition
For the value proposition, we followed the temaplate:

### Customer reachability
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VP-ME1.png)

### Key necessity
The main problem around this context is the disorder, the poor communication and the lack of a standalone platform or tool to handle the day-to-day activities of a micro-enterprise.

### Value proposition
Provide this necessary platform that improves communication, organization, advertisement and efficiency in this daily routine.  
All your organization **centralized** in one **standalone** platform.

## Revenue Model
The revenue model is based on the following items:

### In-App Advertising
We decided to include advertising on the screen that the users see the most, that we assume at first it is going to be the home of the app, where the recommendations of volunteering organizations are going to appear. This will help us with money that we can spend on maintaining the application. If we have a total of 20.000 users monthly and we earned $0.1 dollars for each one, we would obtain $2.000 dollars.

### Freemium model
The main functionalities of the app will be free (Browsing the catalog, adding products and selling them). However, for the vendors, all of the analytics related functionality, such as the sales dashboard, the production planner and the chatbot aren’t free. In order to access those features, the vendors will have to pay a monthly fee. Initially, we could think of a monthly fee of $5 dollars, that with 3000 entrepreneurships can give a revenue of $15.000 dollars.

### Data monetization
The data of the user’s location combined with his preferences can be sold to other companies (Maybe food delivery companies such as Rappi) in order to later create micromarketing. The value of this data depends on the companies.

## Cost model
The cost model is based on the following items:

### Servers, backend engine, etc.
We will have a cost associated with the operation of the application. This cost refers to the hosting and the servers of the backend of our application, which is going to be in the cloud (with fault-tolerance and high scalability to support a large number of users). We will also have costs to publish the app on App Store and Google Play Store, and on maintaining our analytics backend and services.

Assuming that the backend is completely in Firebase:  
With 20.000 users, if we assume that the users make 30 writes to the DB in average, and read from the DB 1750 times per month, and we have and amount of data equivalent to 18GiB, and we store each one of the files and photos and have 40GB of storage in the DB, and users call the serverless part of our backend 500 times per month each one of them, we will get a total monthly cost of $104 dollars (including other costs such as Egress GB transmitted cost, TestLab, etc.).

### Salaries
As a team of 4 members, we estimate that each one of us is going to earn the minimum legal salary in Colombia ($265,52 dollars). This gives us a total of $1062.08 dollars to be paid each month.