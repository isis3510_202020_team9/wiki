# New Personas :man:
Here we did the two prospect users and an analytics persona.

In the first one, we focused on the process from the side of the person that is trying to apply to a volunteering opportunity. Because wealready had a persona that matched that user, we decided to use this persona as our prospective user and do the application according to the necesities of the user.
[Link of the first prospect user](https://drive.google.com/file/d/1HU3Yi6UhRnf1CKypQVmzxsCvLyI1FcAf/view?usp=sharing)  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteerProspectUser1.png)


In the second one, We looked at an user that wants to donate to a noble cause, like a volunteering project.
[Link of the second prospect user](https://drive.google.com/file/d/1KZsWrp6Pnw4rmzYOWMH1B8FonbQKXoFX/view?usp=sharing)  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteerProspectUser2.png)

The last one, which is the analytics one, we focused on the person that need the most data about the process: The Volunteering organization administrative. This person needs to check the evolution of the process through time and also see the metrics about the quantity of donations. Also, the volunteer admintistrator wants to know the type of people that are apllying to the organization. 

[Link of the analytics persona](https://drive.google.com/file/d/1inf0188GK49GLhptSGX-BNvuVQWtdA_6/view?usp=sharing)  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/Analytic_persona_Volunteering.png)


[Link of this persona as a prospect User](https://drive.google.com/file/d/1lWLRxb3fqAsmPdrMpC79esXM9NHS7qvC/view?usp=sharing)  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteeringAsProspectUser3.png)

# PAS :nerd_face:
We transformed the PAS that we developed for the MS3 into a table, adding in more detail each one of the problems. The table built is the following:

| # | Problem | Alternative | Solution |  |
|:-:|:-:|:-:|:-:|-|
| 1 | People have to do a lot of research to find their preferred volunteering organization, leading to time wasted that they could invest in other activities. | The users search for volunteering organizations through the internet (search engines) and social media, an activity that requires a lot of time because the process is completely manual and is the user's responsibility. | Centralized list of volunteering offerings, where the recommendation system shows those offerings based on the users preferences. This recommendation system could also modify the recommendations based on the user's navigation. The user’s profile is matched with the best organization for him (based on location too). This helps the user avoid wasting time. |  |
| 2 | The information of the volunteering organizations is not always right or it is outdated, sometimes the deadlines for applying to these organizations are wrong on their websites. | Calling or contacting through other means (messaging platforms) to the volunteering organization to confirm the information. This leads to time wasted and the process is slowed (could also lead to mistrust). | Information of each one of the volunteering offerings is organized on the recommendation system. The app won’t show offers that are outdated so the user is always informed correctly. |  |
| 3 | It is difficult for people to keep up with the process when they apply to a volunteering organization. | The users use messaging platforms (Messenger, WhatsApp, Email, etc) and applications to communicate and keep up with the process when they apply to volunteer organizations. This means may not be responded easily if they get accumulated. Also, this implies time waiting without knowing the state of their application and could lead to mistakes. | When the user applies, each step of the process can be tracked in the application, showing each one of the stages to the user. This helps the user to be always conscious of his process and avoid mistakes. |  |
| 4 | There are several posts related to donations spread through the internet and especially in social media. This makes them disorganized. | The users navigate through the internet and social media to find donation orgs. or people, an activity that requires a lot of time because the user needs to compare everything he finds. | A recommendation system on the app, helps the user see the best possible donations based on the user’s preferences and the organization needs and location. |  |
| 5 | People have to read long, emotional-based descriptions on different posts to analyze if they want to donate. | They navigate throughout each one of the posts and analyze images and content on it. Users cannot have certainty on the post they choose, other than others recommendations. | The donations are ranked on the system of the app based on the user’s preferences and also raking based on the importance of the donation. This will inform the user the necessity of people when donating and to decide faster. |  |
| 6 | Donating things other than money is tedious and it is challenging to see the impact of the cash donations. | Banking accounts and platforms of crowdfunding such as Vaki are used to collect money from donations, being difficult to donate with goods instead of money. They can ask through messaging platforms to know what happened or visit sites or social media to see somehow the impact of the money. The goods are picked or sent but the user has to manage the process from contacting till giving the goods to donate. | The whole process of contacting and donating is going to be done into the app. The user can select what to donate and coordinate the process with the organization. Also, the impact is going to be shown through the organization’s profile, linking related posts to show the impact that the donations create (this will be mandatory). |  |
| 7 | It is difficult for the volunteering organization to keep track of a high number of applicants at the same time. This makes it hard for a single person to deal with all the applicants. | Different communication and messaging methods are used to reach the applicants. This also leads to mistakes and can be very time-consuming. | The volunteer organizations will have the information organized and classified, depending on which stage the applicant is in. This will help them to contact the applicants faster and to avoid possible mistakes. |  |
| 8 | The volunteering organization doesn’t know where to find interested people.  | They post in different social media the offers to apply to the organizations but this sometimes leads to people applying after the deadline. In their web pages also the information is posted but not everyone sees their websites. | The volunteering organizations are going to be able to post the information related to their hiring in the apps recommendation system, so the system matches the users profiles with those offers based on the user’s preferences. This helps the organizations to invest time wisely and avoid people being informed wrongly. |  |
| 9 | People often ignore the donations posted on social media. | The organization or individuals that post, tend to add images and messages that encourage the user to donate, but sometimes long texts can have the contrary effect. | The system recommends donations that are relevant for each user and simplifies the relevant information to show there. The users that donate can see the impact of their donations. |  |
| 10 | The volunteering organizations do not have statistics on their progress of users or volunteers that they have had. They don’t know how to improve on the organization. | They keep lists in Excel or documents that are difficult to keep or can be lost. | The system will provide the volunteering organizations with a dashboard where they can keep the record of volunteers, frequency, averages and relevant information on them that can make them improve and make wise decisions. |  |

# Context Canvas :scroll:
Here is the context canvas for our solution ([**link to it**](https://drive.google.com/file/d/1kD3rRS5kTFKZcM0M7QWqCP5mecirY2CB/view?usp=sharing)):

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteeringContextCanvas.png)

We made this canvas based on the personas we developed at the beginning and the new personas we did. This context canvas groups everything that we have done up to this moment.

# Business Questions :thinking:
For the questions, we decided to write them in order (From type 1 to type*). We wrote them on a Google Docs to classify them by colour based on their type and also did a classification for the VD Map based on the datasets each question was relating to. This document can be accessed [**here**](https://docs.google.com/document/d/1RhlMFhXXHXKvlGbRF1j5PKyobKw_EH49C-7jzGjUxRI/edit?usp=sharing). The questions are listed below (There's 1 extra question):

1. What is the Android distribution of our client devices? What is the percentage of users that use our platform on an iOS device?
2. What is the percentage of users that have the latest Android and ios operating systems?
3. Does the app load in less than 4 seconds ?
4. Is the app fluent in all its screens (less than 0.2 ms between transitions)?
5. Are the last release bugs lower than our goal (max. 3 bugs) ?
6. How many days passed since a volunteering org. was visited by a volunteer from the app? → Recommend to the users if there are a lot of days.
7. From the volunteering organizations that are presented as recommended to the user, what is the percentage of organizations that the user really explores? 
8. From the nearby volunteering opportunities that appear to the user, what is the proportion of volunteering organizations that the user really explores? 
9. What is the average time that an organization expends on accepting a volunteer? → If the time is greater than X, we should talk with the organization.
10. From the times the user opens the application, in what percentage of times does the user check his donation’s history?
11. What is the average frequency that the user changes his profile information?
12. How many days have passed since a user applied to a volunteering organization? 
13. On average, how much time does the user take to level up? -> We could adjust the amount of xp  gained by volunteering or by donating
14. Which is the percentage of users that redeem each kind of prize?
15. What is the distribution of users that end the volunteering application at early steps of the process? → We could identify blocking steps.
16. For the organizations, what is the best month to launch a donation campaign? In which month do they generate more income?
17. What is the percentage of users that haven’t used our app in the last 15 days?
18. What is the percentage of users which enable their geolocation? 
19. What is the percentage of users that play announcements to donate?  We could know if it’s relevant to this functionality.
20. What is the percentage of users that actually redeem their prizes gained when they level up? 
21. What is the percentage of users that apply to a volunteering opportunity but haven’t made any donations?
22. What is the heatmap of the organizations where users are going?
23. On average, how much time a user spends on an organization?
24. What are the user’s most frequent locations? Does the average user have a regular path?
25. Which are the geographical points where users are more frequent to apply to a volunteering organization?
26. What are the user’s favourite categories on their recommendations ? 
27. On average, which are the hours that a volunteer has validated through our app?
28. What is the progress, on average, in each one of the stages on the overall applications to a volunteering opportunity? How can we present those results to the volunteering admin?
29. On average, at what percentage of completion does the donation process is when x days have passed? 
30. What is the amount of users that contribute with donations after being volunteers?
31. What is the percentage of high-impacting volunteering organizations that the users do not apply to them? 
32. What is the screen where the users stay the most?Ads
33. What is the percentage of acquisition of users from each channel where they downloaded the app?
34. How much income from donations gets a volunteering organization per month? we can use this to get to more organizations.
35. What is the percentage of volunteering organizations that registered on the platform in the first year since our launch?
36. What is the amount of organizations that are connected to Unicef, United Nations or other globally recognized NGO, which are prefered by the users?
37. Which is the location distribution of the organizations? → Recognize new points to add organizations.
38. What is the percentage of users that have the last version of the app?
39. What is the proportion of users that donate physical materials over money? 
40. In which amount users uninstall the app after being volunteers for at least once ?
41. Does the user interact with the dashboard about his information(volunteering hours/month, aptitudes acquired on each volunteering org., etc)? In what percentage? → See if they do it so we can add goals that the user can set up to himself


# VD Map :clipboard:
This is the VD Map developed, based on each one of the 41 questions. The VD Map can also be found on the following [**link**](https://drive.google.com/file/d/1JrzX9RKYUF2xBrdbNreGMqr9OmIQbE6e/view?usp=sharing) The classification can be found on the Google Docs in the Business questions section:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteeringVDMap.png)

In the following map, we focused in displaying all the business questions and relate all the data we consider is relevant for obtaining insights from the data.

# Business Model :chart_with_upwards_trend:
This is the business model that we developed for this idea.

## Value proposition
For the value proposition, we followed the temaplate:

### Customer reachability
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VP-Volunteer1.png)

### Key necessity
This is a problem of two tails. The ONGs need to do a lot of work to recruit volunteers. Also, the possible volunteers are people that don't know how to help and donate. There isn't enough updated information and it is spread around the internet and social media.

### Value proposition
Centralize the information about ONGs and simplify the application and donation processes to possible volunteers.
The key words in this approximation are: centralize and simplify, because the users want an easier and effective process.

## Revenue model
The revenue model is based on the following items:

### In-App Advertising
We decided to include advertising on the screen that the users see the most, that we assume at first it is going to be the home of the app, where the recommendations of volunteering organizations are going to appear. This will help us with money that we can spend on maintaining the application. If we have a total of 20.000 users monthly and we earned $0.2 dollars for each one, we would obtain $10.000 dollars.

### Volunteer donations
The users are going to be able to donate to the developer team, the amount of money they consider. This to help the operation and maintenance of the application. As this application focuses on volunteering and donations, we are not expecting to gain a lot of money with it, just the necessary. Assuming 20.000 users monthly, we could obtain an average of $500 dollars if the 2.5% of our users donate 1 dollar each.

### Data monetization
We can sell the data to the United Nations or another organization that’s interested in how volunteers are contributing to social problems in Colombia, and even in the world if our platform is used outside the country. This will also help the operation and maintenance of the application. The amount of money earned from this depends on the organization.

## Cost model
The cost model is based on the following items:

### Servers, backend engine, etc.
We will have a cost associated with the operation of the application. This cost refers to the hosting and the servers of the backend of our application, which is going to be in the cloud (with fault-tolerance and high scalability to support a large number of users). We will also have costs to publish the app on App Store and Google Play Store, and on maintaining our analytics backend and services.

Assuming that the backend is completely in Firebase:  
With 20.000 users, if we assume that the users make 30 writes to the DB in average, and read from the DB 1750 times per month, and we have and amount of data equivalent to 18GiB, and we store each one of the files and photos and have 40GB of storage in the DB, and users call the serverless part of our backend 500 times per month each one of them, we will get a total monthly cost of $104 dollars (including other costs such as Egress GB transmitted cost, TestLab, etc).

### Salaries
As a team of 4 members, we estimate that each one of us is going to earn the minimum legal salary in Colombia ($265,52 dollars). This gives us a total of $1062.08 dollars to be paid each month.