# PAS :capital_abcd:
Here, we transformed the PAS that we developed for the MS3 into a table, which is the following:

| # | Problem | Alternative | Solution |
|:-:|:-:|:-:|:-:|
| 1 | It takes a lot of time to reply to every customer that asks for an order in a WhatsApp chat. | Reply them as fast as possible, despite having a lot of chats constantly active. Also, we might be working in another order at the time, and it needs to be interrupted to reply them | A new chat methodology that involves a bot to reply to new users automatically, and take the orders as well. This saves a lot of time replying to each user. |
| 2 | There is a need for promoting microenterprises,or there won't be any clients that buy from us and support our cause. | Rely on Instagram free ads and joining WhatsApp entrepreneur groups to promote my business, but these are a little disorganized and doesn’t ensure reaching enough people and that these people will read these posts anyway | An independent platform with its own particular feed which is exclusive for these kinds of posts, and I can post knowing it won't be lost in a chat like those WhatsApp groups mentioned, but is correctly stored in the app feed. This gives a special space for these promotions to be shown. |
| 3 | We need to know what are the micro-enterprises nearby my location so I know what are my competition, and more than that, what are my possible alliances  | Having the contacts of some neighbours on our phones and Facebook help out, and this provides connections on WhatsApp and Instagram allowing very limited knowledge about their activity on their micro-enterprises | With a platform designed for micro-enterprises, there is no limit in knowing about only the people we have on our contacts, but to any user that has the app nearby our location, and this feed will show filtered posts as needed.  |
| 4 | We need to receive the payment from our users with more methods other than just cash, something easy, fast and reliable | Currently the most used methods are bank transfers or virtual pockets such as Nequi or RappiPay | Including robust and generic payment methods such as PSE will be a huge plus because we don't limit the user with specific banks or virtual pockets. This enables more payment methods available for customers. |
| 5 | There is a need of knowing what is the opinion of our customers to find improvement points in our products/services | We rely on thanking messages of the customers that ordered anything and comments on ads posted in social media to get an idea of people’s perception. | A rate system in a standalone platform that allows customers to easily give 1-5 starts to a micro-enterprise that serves an order to them. This will result in direct feedback based on a numeric system. |
| 6 | When there are “peculiarities” in one particular order, it is difficult to attend to this when there are a lot of other orders going on in other chats. | Place policy like “After an order is set, there is no way to change it”. Alternatively, we can constantly check WhatsApp (consumes a lot of time) waiting to encounter a case like this | In the chatbot proposed in idea #1, add an option as “ask directly” when special cases like this occur, so that when clicked is notified to us and we can directly attend the customer. This allows customers to make their requests with a human directly. |
| 7 | When providing a one-time kind of service, in most cases customers lose track of the micro-enterprise and we don’t get to know about them or reach them anymore | Adding customers to a mailing list or ask them to like our page in Facebook or Instagram | Add a feature such as “follow site” so that users always get their posts, and propose these after they rated the service they paid for. This allows options for more consistent connections between micro-enterprises and customers. |
| 8 | As a customer, is sometimes difficult to find what you actually need, given that many micro-enterprises post in massive groups or feeds, and is not easy to find what you are actually looking for | Saving the posts at the moment you see them and store them with a label on a notepad or sticky | Adding a label system that tags each post with a specific category, making the filtering process easier for customers. This allows in-app to categorize posts for the customer. |
| 9 | We don’t know a lot of our customers, usually is just communication for an order with a random number on WhatsApp | We just ask for the name and billing information to deliver the product or reach for the service. | With profiles in our platform, we might have a better idea of who our customers are, given the closer communication. |
| 10 | We want to constantly improve the user experience with all the process (ordering, paying, delivering, rating), and is difficult if many people have different perceptions and don’t communicate them | Read the negative comments or constructive criticism in posts we made in social media | Add an option to send textual comments from the user in such an appealing way that is difficult to just skip that step. This gives the option of more detailed feedback for each micro-enter |

# Context canvas :traffic_light:
Here is the context canvas for our solution:  
[Context canvas link](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/Deliverable/img/ContextCanvasME.png)
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/Deliverable/img/ContextCanvasME.png)

We made this canvas based on the personas we developed at the beginning and the new personas we did. This context canvas groups everything that we have done up to this moment.

# Personas :man: :woman:
Here we did **two** aditional personas and an extra one, which refers to the analytics persona. Also is important to remind that at the past microsprint we did one persona([First Persona Link](https://drive.google.com/file/d/1NDRXe5fGvPyvwPHeWdkflIkF8vjO2eBy/view?usp=sharing))

In the first one, we are talking about a prospect persona that would use our solution. This persona needs the solution in order to sell more products and have the best rated cakes.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/PersonaMicroEnterprises.png)

In the second one,([Persona Link](https://drive.google.com/file/d/1txF5mltuetPldfOFhlKr5xGWre6x53n8/view?usp=sharing)) we have also a prospect user but not the one who wants to sell products. This persona wants to find the best products at the lower possible price. Also, she desired to help other entreperneurs to grow and improve.  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/PersonaMicroEnterprises2.0.png)

In the third one,([Persona Link](https://drive.google.com/file/d/1GI6RYpsTN4LJ4gUCcDKeE2ctL6WC-SdQ/view?usp=sharing)) we have to talk about an enterperneur. This type of user center his objectives in the income and a good customer review.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/PersonaMicroEnterprises3.png)

The last one, ([Persona Link](https://drive.google.com/file/d/1TIgyXTZvolcBrm77G7PWS9jyME7h4gPu/view?usp=sharing)) which is the analytics one, is that type of user that wants to improve his business based on data. He know that the information is the most important thing in the world.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/Analytic%20persona%20MicroEnterprises.png)

# Empathy Maps :bookmark_tabs:
For this particular problem, we seek to be able to characterize who the affected users are and how it affects them. This process occurred organically thanks to the interviews we conducted. Those interviews were generated during the brainstorming process because it was crucial to make hypotheses and contrast with real affected users. Also, the interviews allow us to refine or reject those assumptions to build empathy maps that seriously empathize and not just portray team positions.
In particular, these interviews allowed us to identify various types of users and various ways in which they are affected and solved by the problem in their day-to-day life.
On the one hand, we have the testimonies of two entrepreneurs. Despite undertaking and feeling identified with the problems that we will discuss below, their approach is different.  
The first of them is an entrepreneur who sells biscuits, cakes, and sweets. For her, how she sells and how many people she sells her assets to is the most important thing. She believes that the problem is that the current platforms on which an order is created and completed are not the right ones. From receiving many messages and not being able to process them instantly to the fact of not being able to reach more clients due to the limitations they imply [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598283849958/88809e2656d4ce4a3773850c7c887ce2864d1a83)  
The other case is an entrepreneur focused on service: tutorials for programmers. Although it is a service, there are similarities with the previous testimony. She also believes that these means are not the best to reach people. However, your concerns are more about the way you provide your service and not the chain of events that happen before it. [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598283866656/0fad7509066243f8e93706ab790982f6a162e9d4)  
In this way, upon discovering various ventures, our approach gradually focused on the first type of user. The one who is more interested in the contact, order, delivery, and payment process.

However, the entrepreneurial person is not our only one involved in the problem. It is also important to refer to the user who wants the products or services of the companies: the consumer.
In this user, the consensus is usually easier. Most buyers perceive how the purchasing processes are very slow. Although it is not only about the sale but about the communication with the company, sometimes they take days to respond. On the other hand, consumers perceive that there is no concrete way to trust companies. They do not have ratings so the only way to learn a little more is from comments on their social networks or recommendations from family and friends.

### Empathy Map 1: [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598665720542/6e698d175af77ef1ca7f38f33d0bd825819a0cd7)
This empathy map is related to a seeking customer. The main insights from this are the necessity of acquire those type of products to help the ventures to grow.  
If you want to hear the interview: [_Laura interview_](https://drive.google.com/file/d/1uV_DCDH31_5ODocVS3yYuiSrY55JBRGl/view?usp=sharing)
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Ventures%20EM-%20Customer.png)
### Empathy Map 2: [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles4123/1598444588567/c6f4a31900f228ac41c0df6f83fa52f409ac48e1)
This empathy map is related to a casual customer. The main insights from this is that users are constantly hitted with these kinds of businesses every day throw social media, and most of the time is something interesting to acquire/hire.
The information of the interview was only processed for the empathy map, the interviewed person didn't allowed us to store the literal information she provided.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Ventures%20EM%20-%20Customer%20Old%20Woman.png)
### Empathy Map 3: [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598283849958/88809e2656d4ce4a3773850c7c887ce2864d1a83) 
This empathy map is related to the second user, the micro-enterprise members, in this case that provides a product. The interviewed is a 27 year old college student that is baking cakes as her micro-enterprise during this quarantine.
You can see the entire study of this case here, at the individual interview section: [_Luisa's interview_](https://gitlab.com/isis3510_202020_team9/wiki/-/blob/master/Sprint%201/MS2/Emprende%20Plus.md)
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Ventures%20EM%20-%20Microenterprise.png)
### Empathy Map 4: [*Link to empathy map*](https://app.mural.co/t/moviles6247/m/moviles6247/1598283866656/0fad7509066243f8e93706ab790982f6a162e9d4)
This empathy map is based on a micro-enterprise member that provides a service. The interviewed is a 21 year old college student that does coding tutoring in his free time since his second semester.
If you want to hear the interview: [_Carlos interview_](https://drive.google.com/file/d/1_0xWF6k3IpB7rHAD22ywgnVoMLKvSATD/view?usp=sharing)
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/Ventures%20EM%20-%20Microenterprise%20(services).png)


# VD Maps :pushpin:
This is the VD Map developed, based on each one of the 40 questions. We decide to classify the questions in possible databases and the process.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/classification-bq-me.png)

Here, we have the VD map of those grouped questions ([VD Map](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VD%20Map%20Microenterprises.png)):

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VD%20Map%20Microenterprises.png)

# Business questions :telescope:
[Document link](https://docs.google.com/document/d/1RhlMFhXXHXKvlGbRF1j5PKyobKw_EH49C-7jzGjUxRI/edit?usp=sharing)  
For this process, we hold ideation and classification sessions. In the first place, we can see the categories referenced with colors as follows:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/Categorization-Business%20questions.png)


![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/microenterprises-categ.png)

The questions are listed below:  
1. What is the Android distribution of our client devices? What is the percentage of users that use our platform on an iOS device?
2. What is the average RAM used by the app each minute?
3. Does the app load in less than 4 seconds?
4. What is the monthly amount of crashes for each screen?
5. What is the probability that a user with a phone from 2017 or less has crashes ?
6. How many days have passed since the client last used the app?
7. How much time on average does the user spend browsing the catalog of microenterprises?
8. How long does it take to order a product, in average?
9. Does the user have to leave the app to use any of the payment methods? How frequently?
10. What is the average time to deliver the client a product, and how much is the average cost of this delivery? 
11. What is the distribution of preferred categories per location?Will there be separate panels for different categories?
12. How frequently do the vendors post images of their products in the app? 
13. How many products/services is each vendor offering?
14. What is the percentage of each channel that is presented in the app to communicate with the client? (Whatsapp, phone, Instagram, chatbot)How can a user communicate faster and easier with a micro-enterprise?
15. How can you share an app post through external social media? Which social media sharing is the most used?
16. How much percentage of users is the minimum for distance filtering to be a relevant feature for them? How much percentage of users would be if we include user’s reviews organization instead?
17. What's the percentage of users that use the in-app payment, and what's the other percentage that are paying separately?
18. What percentage of orders couldn’t be resolved by the chatbot?
19. What percentage of users use the camera scanner for their receipts?
20. What percentage of vendors are looking at the analytics insights?
21. Do we have some information about user preferred product categories? Which are those?
22. Do we have information about users' average monthly expenses? What is that average?
23. Do we have information about the time that an order takes to be delivered? What’s that time on average?
24. Do we have information about the user comments on their orders? → Yes, we can process that text to user feelings.
25. Do we have information about the users perception about the ventures with a quantitative rate?
26. Do we have information about the proportion (by gender) of users that buy products in the app?
27. Do we have information about the age of the users in order to classify and relate their favorite products?
28. Do we have information about the historic orders? Yes, we could analyze the best days/hours for producing.
29. What is the percentage of entrepreneurships that gain enough to pay taxed to the government? → Sell the data to the DIAN
30. Do we have information about the location of the users that frequently buy products through the app? → We can sell this to the delivery apps like Rappi, to send more people to that area
31. How many days passed since an order greater than 50mil pesos was charged?  
32. What percentage of Vendors haven’t sold anything on our platform?
33. What percentage of orders get cancelled daily?	
34. What percentage of users have bought at least once in the app?
35. Our revenue comes from orders, could we make cobranding to introduce promotions?What percentage of users are selling more than one thing in a product (combos)?
36. What is the price distribution of the products per category? → Could be useful to implement new functionality
37. Would including sales of products outside the app make the user more interested in buying? How frequently do users search for a product that isn’t available?
38. How can the app handle refunds in case of not successful orders? How frequently does this happen? 
39. Is the app fluent in all its  screens (less than 0.2 ms between transitions)?
40. Are the last release bugs lower than our goal (max. 3 bugs) ?

# Functional scenarios :beginner:
These are the functional scenarios we defined for the Micro-Enterprise problem:

## List all the in progress order

| Scenario ID | F-SCE-01 |
|-|-|
| Scenario Name | List all the in progress orders |
| Description | As a venture I want to see all the current orders, ordered by date. *Path*: I want to click the orders sections and watch enough information of everyone. Also, I want to see them ordered by date or by price because it is necessary to manage the priority of the orders. |
| Priority | 5 |
| Complexity | 2 |

## See the details of an order.

| Scenario ID | F-SCE-02 |
|-|-|
| Scenario Name | See the details of an order. |
| Description | As a venture I want to see specific order details like the products associated, the desired date of delivery and more. Path: I want to click an order and see the details of the order to clarify what is needed. |
| Priority | 5 |
| Complexity | 2 |

## Reject an order

| Scenario ID | F-SCE-03 |
|-|-|
| Scenario Name | Reject an order |
| Description | As a venture I want to reject an order if it is necessary. *Path*: I see the order detail and take the decision to reject the order. I put a comment telling the reasons. Also, the client is notified. |
| Priority | 4 |
| Complexity | 3 |

## Accept an order

| Scenario ID | F-SCE-04 |
|-|-|
| Scenario Name | Accept an order |
| Description | As a venture, I want to accept an order (definitely). Path: I want to select a specific order, see its details and accept it. Then, the client is notified and the order status is changed to in progress. |
| Priority | 5 |
| Complexity | 3 |

## Filter the orders by different criteria

| Scenario ID | F-SCE-05 |
|-|-|
| Scenario Name | Filter the orders by different criteria |
| Description | As a venture I want to filter the orders by the client, status, date and more.  *Path*: I see the orders and I want to filter by different criteria to visualize them more effective and easier. |
| Priority | 2 |
| Complexity | 3 |

## Recommendation of ventures based on customers location and preferences

| Scenario ID | F-SCE-06 |
|-|-|
| Scenario Name | Recommendation of ventures based on customers location and preferences |
| Description | As a customer, I want to get recommendations of ventures based on my location or my preferences so that I can choose the best option to buy products in. *Path*: I will see the recommendations that the app has for me so that I can choose the best option. These recommendations are based on location or my preferences. |
| Priority | 3 |
| Complexity | 5 |

## Searching of ventures

| Scenario ID | F-SCE-07 |
|-|-|
| Scenario Name | Searching of ventures |
| Description | As a customer, I want to search for different ventures based on their type (category) or their name, so that I can filter and choose the option that I like the most or that I want to find. *Path*: I want to filter the ventures by different criteria and the app filters based on type of ventures and name of ventures. |
| Priority | 2 |
| Complexity | 3 |

## Buy a product of a venture

| Scenario ID | F-SCE-08 |
|-|-|
| Scenario Name | Buy a product of a venture |
| Description | Given that I selected a venture and a product, I want to buy the product so that I can get it and use it. *Path*: I select a venture and on the selected product. After that, I see the product and its details and I buy it. The payment methods appear and the order is finished (filling the details like name, email, phone for the invoice). Then I can see the order is completed and the process of delivery starts. The other option is to buy through a chatbot, which based on a conversation the chatbot helps me to create the order for the product I want to buy. |
| Priority | 5 |
| Complexity | 4 |

## Search products of a venture

| Scenario ID | F-SCE-09 |
|-|-|
| Scenario Name | Search products of a venture |
| Description | Given that I selected a venture, I want to search for products on it so that I can find the one I want to buy.  *Path*: I select a venture and then filter the products by its name. Then the filtering is done while I write. |
| Priority | 2 |
| Complexity | 2 |

## Track the progress of an order

| Scenario ID | F-SCE-10 |
|-|-|
| Scenario Name | Track the progress of an order |
| Description | Given that I already bought a product of a venture, I want to track the status/progress of the order that was created, so that I can know in which stage my product is.*Path*: I select  the order that was created and then see the stage in which it is (preparation, in progress, in delivery or completed). I can contact the venture on the chat. |
| Priority | 4 |
| Complexity | 3 |

## Import/Take images for a post

| Scenario ID | F-SCE-11 |
|-|-|
| Scenario Name | Import/Take images for a post |
| Description | I want to be able to take pictures directly or import pictures from my gallery into the app to create a new product/service available. *Path*: Inside the product/service creation, I can access the gallery or the camera from my phone so that I can get the pictures for the service/product. |
| Priority | 4 |
| Complexity | 5 |

## Create a post of a product/service

| Scenario ID | F-SCE-12 |
|-|-|
| Scenario Name | Create a post of a product/service |
| Description | With the images imported, I want to be able to create a post with the new product I’m intending to sell. *Path*: After selecting the images, I will fill the fields required like name, description, etc. to complete the product/service post. |
| Priority | 4 |
| Complexity | 3 |

## List all my products/services

| Scenario ID | F-SCE-13 |
|-|-|
| Scenario Name | List all my products/services |
| Description | I want to see all the products/services I have available to offer currently. *Path*: From my profile as a venture, I can see all the products/services I’ve posted in the app to be sold. |
| Priority | 5 |
| Complexity | 3 |

## Remove a product/service

| Scenario ID | F-SCE-14 |
|-|-|
| Scenario Name | Remove a product/service |
| Description | I want to remove a product/service since I wont be offering it anymore. *Path*: When I’m seeing all my products/services, I can delete a product to remove it from the available offers. |
| Priority | 5 |
| Complexity | 2 |

## Add discount

| Scenario ID | F-SCE-15 |
|-|-|
| Scenario Name | Add discount |
| Description | I want to include a discount manually to a product/service on my list.*Path*: While I’m seeing the list of products/services that I offer, I can add a discount to a product, set a percentage of discount for it and explain the reason and limitations of the discount. |
| Priority | 3 |
| Complexity | 2 |

## Enter the analytics platform

| Scenario ID | F-SCE-16 |
|-|-|
| Scenario Name | Enter the analytics platform |
| Description | As a venture owner, I want to enter the analytics platform for my respective venture.   |
| Priority | 5 |
| Complexity | 1 |

## Determine costs

| Scenario ID | F-SCE-17 |
|-|-|
| Scenario Name | Determine costs |
| Description | For each of the products that I as a venture owner have, I want to input the costs per product manually or by scanning the invoices of the materials to determine the total costs. |
| Priority | 3 |
| Complexity | 3 |

## Track Income

| Scenario ID | F-SCE-18 |
|-|-|
| Scenario Name | Track Income |
| Description | For each of the products that I as a venture have, I want to see the timeline of orders and the moments where I have generated the most income. Also, I would like to check if the income from sales is enough to meet my goals each month. |
| Priority | 5 |
| Complexity | 5 |

## Plan Production

| Scenario ID | F-SCE-19 |
|-|-|
| Scenario Name | Plan Production |
| Description | After receiving orders, I would like the app to prioritize how much of each product do I have to do and how much materials do I need each month according to the demand in order to spend the right amount. If necessary, I could schedule the production hours beforehand. |
| Priority | 5 |
| Complexity | 4 |

## Get insights about clients

| Scenario ID | F-SCE-20 |
|-|-|
| Scenario Name | Get insights about clients |
| Description | After creating a venture, I would like to see insights about the growth of clients each week, the demographics of those clients and the reviews they have made about my venture and my products so that I can offer a better service.  |
| Priority | 5 |
| Complexity | 4 |

# Quality scenarios :diamond_shape_with_a_dot_inside:
These are the functional scenarios we defined for the Micro-Enterprise problem:

## Lack of internet connection during a Payment processing

| Scenario ID | Q-SCE-01 |
|-|-|
| Scenario Name | Lack of internet connection during a Payment processing |
| Quality Attributes | Connectivity, resilience |
| App Status and Context | The mobile app is requesting data for executing a transaction. |
| Changes in the context | The user loses his connectivity, then the data connection is lost. |
| System Reaction | The app notifies the user about the poor connection and the transaction is moved for resuming later. |

## Unavailability of GPS Connection

| Scenario ID | Q-SCE-02 |
|-|-|
| Scenario Name | Unavailability of GPS Connection |
| Quality Attributes | Eventual connectivity |
| App Status and Context | The app requests for the user geolocation using the corresponding API. |
| Changes in the context | The API answers with the user’s last location in the best case. In the worst case the API answers with no location. |
| System Reaction | The system uses the user’s last location to show recommendations based on it (Informing the user that those recommendations were given using his/her last location). If no location is given, the app shows recommendations based on the last stored/cached location of the user. If there’s no cached nor last location retrieved, the app shows a default recommendation list and informs the user that they are not recommended using his location. |

## Functionality During Low Battery Mode

| Scenario ID | Q-SCE-03 |
|-|-|
| Scenario Name | Functionality During Low Battery Mode |
| Quality Attributes | Performance |
| App Status and Context | The user’s device is getting low on battery, so the user decides to activate the low battery saving mode. This mode disables background data usage, and delays some notifications.  |
| Changes in the context | The energy of the cell phone begins to get lower and the resources available by the OS are limited. |
| System Reaction | Some results and recommendations aren’t as precise and the actualizations are less frequent. The background processes stop and the state of the app is saved. |

## RAM Memory Pressure on the Phone

| Scenario ID | Q-SCE-04 |
|-|-|
| Scenario Name | RAM Memory Pressure on the Phone |
| Quality Attributes | Performance, Resilience |
| App Status and Context | The app is showing the feed of recommended volunteering organizations, the user has other apps open that are also consuming memory. Finally the user puts the volunteering app in the background. |
| Changes in the context | The Viking Killer in the Android Os begins to kill the processes. |
| System Reaction | To avoid issues when moving the app from background to foreground, the state is saved. |

## ROM Memory Pressure on the Phone

| Scenario ID | Q-SCE-05 |
|-|-|
| Scenario Name | ROM Memory Pressure on the Phone |
| Quality Attributes | Performance |
| App Status and Context | The mobile app has stored various posts as cache in case is not refreshed. The user installed new apps and the phone notified to start deleting cache info from certain apps, and might ask the user to allow uninstall automatically. |
| Changes in the context | There OS might remove those cached posts to clear storage |
| System Reaction | The app must download new posts each time is opened, until there is more free space to store cached posts in ROM. |

## Problems loading the screens because of overdrawing, resizing, main thread overloaded

| Scenario ID | Q-SCE-06 |
|-|-|
| Scenario Name | Problems loading the screens because of overdrawing, resizing, main thread overloaded |
| Quality Attributes | Latency, Performance |
| App Status and Context | The mobile app is requesting the information to create an order but the screen is white. |
| Changes in the context | There is a problem with a front end feature and the app is not answering. |
| System Reaction | The app should immediately put a loading screen using another thread different from the main one, because that one may be busy and can’t render correctly the graphical components in the screen. After some time, tell the user that the feature is unavailable if it wasn’t able to work and give the option to return to the last functional screen. |

## Unavailability to access the Database

| Scenario ID | Q-SCE-07 |
|-|-|
| Scenario Name | Unavailability to access the Database |
| Quality Attributes | Availability |
| App Status and Context | The database service stopped working for networking issues from the cloud provider, and it can’t be reached by the app at a certain moment. The app is in regular conditions and can execute any request at any time. |
| Changes in the context | The app will try to access the database through a service of the API and it will return an error, given that at the moment of connecting to the database this wasn’t found in the network. |
| System Reaction | The app, in the best of cases, shows cached information (such as old posts, old search results, etc.). In the worst case, it shows an error message showing that the service is unavailable at the moment. |

## Unavailability to connect to an external API

| Scenario ID | Q-SCE-08 |
|-|-|
| Scenario Name | Unavailability to connect to an external API |
| Quality Attributes | Availability |
| App Status and Context | The venture app is trying to connect to an external API, such as the google maps API, but the service is not responding for them.  |
| Changes in the context | A 50X error was emitted. The service is unavailable. |
| System Reaction | The app should notify the user that the request wasn’t possible and ask to try again. |

## Managing of transactions in order processing

| Scenario ID | Q-SCE-09 |
|-|-|
| Scenario Name | Managing of transactions in order processing |
| Quality Attributes | Concurrency  |
| App Status and Context | Different users are trying to order the same product in a short period of time, sending requests to the database of the application |
| Changes in the context | The application’s database receives all the requests corresponding to the user’s orders of the same product offer and tries to store them. |
| System Reaction | The system is going to work with transactions, so even though the requests were made almost simultaneously, the database will accept them in order. If the product stock is reached on an order, the application will show a message of error to the user informing that the product is unavailable. |

## Possibility of a Data Leak

| Scenario ID | Q-SCE-10 |
|-|-|
| Scenario Name | Possibility of a Data Leak |
| Quality Attributes | Security |
| App Status and Context | The user registers on the application or logs in to use the application. However, another application is trying to access his personal information. |
| Changes in the context | The user’s information is stored internally on the phone. |
| System Reaction | To secure the user’s information, this internal storage of his sensitive/personal data is encrypted so that other applications are not able to read and process it. The malign software is not granted access to the system data and in the case the foreign app accesses the data, it won’t be able to interpret or use it due to the encryption. |

## Ordering from an inconsistent post

| Scenario ID | Q-SCE-11 |
|-|-|
| Scenario Name | Ordering from an inconsistent post |
| Quality Attributes | Availability, Resilience |
| App Status and Context | An user is trying to order from a post that was either cached or stored with a tag by himself, but the venture from this post is no longer available or deleted their account |
| Changes in the context | The ordering process might be laggy given that the system will not be able to find certain data from the venture, given that it doesn’t exist |
| System Reaction | After a loading icon shows, it will show a message saying that the product/service is unavailable and the post will be deleted from the feed, denying any purchase order. |

## Performance during a heavy loading list

| Scenario ID | Q-SCE-12 |
|-|-|
| Scenario Name | Performance during a heavy loading list |
| Quality Attributes | Performance, GUI responsiveness |
| App Status and Context | The app requests a list of elements (products/services, ventures, etc) to view on the catalog and it is retrieved. |
| Changes in the context | The list of elements that was requested by the user quen entering a screen is rendered on the screen and the user starts to scroll through it. |
| System Reaction | To avoid the delay and the poor performance of the application, if the list is too long, the application will perform as an infinite scroll list. This means that the elements that the user is not seeing in the screen are not pre-rendered. Those elements render while the user is near to them so that the performance of the app is not affected. |

## High amount of parallel users during peak times.

| Scenario ID | Q-SCE-13 |
|-|-|
| Scenario Name | High amount of parallel users during peak times. |
| Quality Attributes | Scalability |
| App Status and Context | There is a special occasion  (Saint Valentine’s Day) and many users are trying to order the different products and gifts through our app. The mobile app sends simultaneous requests to the backend service. |
| Changes in the context | The amount of requests that the backend is trying to resolve is increasing. |
| System Reaction | The backend has a multithreading reception system that helps to handle multiple requests at the same time. The user will see a loading icon until the request finishes. Also, the additional servers are deployed to manage the increased number of requests. |

## Multiple spamming on the same button

| Scenario ID | Q-SCE-14 |
|-|-|
| Scenario Name | Multiple spamming on the same button |
| Quality Attributes | Performance |
| App Status and Context | The mobile app has an available button at a particular screen. |
| Changes in the context | The user starts to click repeated times over the button. |
| System Reaction | The app conserves its initial status and the button is locked for any additional activity until the existing process is fulfilled. |

## Change in Font Size

| Scenario ID | Q-SCE-15 |
|-|-|
| Scenario Name | Change in Font Size |
| Quality Attributes | Accessibility |
| App Status and Context | The mobile app is designed with certain dimensions for spacing and fonts. |
| Changes in the context | The user has a default configuration that increases/ decreases the font size. |
| System Reaction | The mobile app adapts and orders again the interface to provide a great user experience. |

# Business model and value proposition :trident:
This is the lean canvas developed to represent the business model:

## Value proposition
For the value proposition, we followed the temaplate:

### Customer reachability
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VP-ME1.png)

### Key necessity
The main problem around this context is the disorder, the poor communication and the lack of a standalone platform or tool to handle the day-to-day activities of a micro-enterprise.

### Value proposition
Provide this necessary platform that improves communication, organization, advertisement and efficiency in this daily routine.  
All your organization **centralized** in one **standalone** platform.

## Revenue Model
The revenue model is based on the following items:

### In-App Advertising
We decided to include advertising on the screen that the users see the most, that we assume at first it is going to be the home of the app, where the recommendations of volunteering organizations are going to appear. This will help us with money that we can spend on maintaining the application. If we have a total of 20.000 users monthly and we earned $0.1 dollars for each one, we would obtain $2.000 dollars.

### Freemium model
The main functionalities of the app will be free (Browsing the catalog, adding products and selling them). However, for the vendors, all of the analytics related functionality, such as the sales dashboard, the production planner and the chatbot aren’t free. In order to access those features, the vendors will have to pay a monthly fee. Initially, we could think of a monthly fee of $5 dollars, that with 3000 entrepreneurships can give a revenue of $15.000 dollars.

### Data monetization
The data of the user’s location combined with his preferences can be sold to other companies (Maybe food delivery companies such as Rappi) in order to later create micromarketing. The value of this data depends on the companies.

## Cost model
The cost model is based on the following items:

### Servers, backend engine, etc.
We will have a cost associated with the operation of the application. This cost refers to the hosting and the servers of the backend of our application, which is going to be in the cloud (with fault-tolerance and high scalability to support a large number of users). We will also have costs to publish the app on App Store and Google Play Store, and on maintaining our analytics backend and services.

Assuming that the backend is completely in Firebase:  
With 20.000 users, if we assume that the users make 30 writes to the DB in average, and read from the DB 1750 times per month, and we have and amount of data equivalent to 18GiB, and we store each one of the files and photos and have 40GB of storage in the DB, and users call the serverless part of our backend 500 times per month each one of them, we will get a total monthly cost of $104 dollars (including other costs such as Egress GB transmitted cost, TestLab, etc.).

### Salaries
As a team of 4 members, we estimate that each one of us is going to earn the minimum legal salary in Colombia ($265,52 dollars). This gives us a total of $1062.08 dollars to be paid each month.