# PAS :capital_abcd:
We transformed the PAS that we developed for the MS3 into a table, adding in more detail each one of the problems. The table built is the following:

| # | Problem | Alternative | Solution |  |
|:-:|:-:|:-:|:-:|-|
| 1 | People have to do a lot of research to find their preferred volunteering organization, leading to time wasted that they could invest in other activities. | The users search for volunteering organizations through the internet (search engines) and social media, an activity that requires a lot of time because the process is completely manual and is the user's responsibility. | Centralized list of volunteering offerings, where the recommendation system shows those offerings based on the users preferences. This recommendation system could also modify the recommendations based on the user's navigation. The user’s profile is matched with the best organization for him (based on location too). This helps the user avoid wasting time. |  |
| 2 | The information of the volunteering organizations is not always right or it is outdated, sometimes the deadlines for applying to these organizations are wrong on their websites. | Calling or contacting through other means (messaging platforms) to the volunteering organization to confirm the information. This leads to time wasted and the process is slowed (could also lead to mistrust). | Information of each one of the volunteering offerings is organized on the recommendation system. The app won’t show offers that are outdated so the user is always informed correctly. |  |
| 3 | It is difficult for people to keep up with the process when they apply to a volunteering organization. | The users use messaging platforms (Messenger, WhatsApp, Email, etc) and applications to communicate and keep up with the process when they apply to volunteer organizations. This means may not be responded easily if they get accumulated. Also, this implies time waiting without knowing the state of their application and could lead to mistakes. | When the user applies, each step of the process can be tracked in the application, showing each one of the stages to the user. This helps the user to be always conscious of his process and avoid mistakes. |  |
| 4 | There are several posts related to donations spread through the internet and especially in social media. This makes them disorganized. | The users navigate through the internet and social media to find donation orgs. or people, an activity that requires a lot of time because the user needs to compare everything he finds. | A recommendation system on the app, helps the user see the best possible donations based on the user’s preferences and the organization needs and location. |  |
| 5 | People have to read long, emotional-based descriptions on different posts to analyze if they want to donate. | They navigate throughout each one of the posts and analyze images and content on it. Users cannot have certainty on the post they choose, other than others recommendations. | The donations are ranked on the system of the app based on the user’s preferences and also raking based on the importance of the donation. This will inform the user the necessity of people when donating and to decide faster. |  |
| 6 | Donating things other than money is tedious and it is challenging to see the impact of the cash donations. | Banking accounts and platforms of crowdfunding such as Vaki are used to collect money from donations, being difficult to donate with goods instead of money. They can ask through messaging platforms to know what happened or visit sites or social media to see somehow the impact of the money. The goods are picked or sent but the user has to manage the process from contacting till giving the goods to donate. | The whole process of contacting and donating is going to be done into the app. The user can select what to donate and coordinate the process with the organization. Also, the impact is going to be shown through the organization’s profile, linking related posts to show the impact that the donations create (this will be mandatory). |  |
| 7 | It is difficult for the volunteering organization to keep track of a high number of applicants at the same time. This makes it hard for a single person to deal with all the applicants. | Different communication and messaging methods are used to reach the applicants. This also leads to mistakes and can be very time-consuming. | The volunteer organizations will have the information organized and classified, depending on which stage the applicant is in. This will help them to contact the applicants faster and to avoid possible mistakes. |  |
| 8 | The volunteering organization doesn’t know where to find interested people.  | They post in different social media the offers to apply to the organizations but this sometimes leads to people applying after the deadline. In their web pages also the information is posted but not everyone sees their websites. | The volunteering organizations are going to be able to post the information related to their hiring in the apps recommendation system, so the system matches the users profiles with those offers based on the user’s preferences. This helps the organizations to invest time wisely and avoid people being informed wrongly. |  |
| 9 | People often ignore the donations posted on social media. | The organization or individuals that post, tend to add images and messages that encourage the user to donate, but sometimes long texts can have the contrary effect. | The system recommends donations that are relevant for each user and simplifies the relevant information to show there. The users that donate can see the impact of their donations. |  |
| 10 | The volunteering organizations do not have statistics on their progress of users or volunteers that they have had. They don’t know how to improve on the organization. | They keep lists in Excel or documents that are difficult to keep or can be lost. | The system will provide the volunteering organizations with a dashboard where they can keep the record of volunteers, frequency, averages and relevant information on them that can make them improve and make wise decisions. |  |
# Context canvas :traffic_light:
Here is the context canvas for our solution ([**link to it**](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/Deliverable/img/ContextCanvasVO.png)):

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/Deliverable/img/ContextCanvasVO.png)

We made this canvas based on the personas we developed at the beginning and the new personas we did. This context canvas groups everything that we have done up to this moment.
# Personas :man: :woman:
Here we did three prospect users and an analytics persona.

In the first Prospect User, we focused on the process from the side of the person that is trying to apply to a volunteering opportunity. Because we already had a persona that matched that user, we decided to use this persona as our prospective user and do the application according to the necesities of the user. This persona wants to apply to a volunteering organization and wants to help as much as possible. 
[Link of the first prospect user](https://drive.google.com/file/d/1HU3Yi6UhRnf1CKypQVmzxsCvLyI1FcAf/view?usp=sharing)  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteerProspectUser1.png)


In the second prospect user, We looked at an user that wants to donate to a noble cause, like a volunteering project. This user is concerned by the current situation of the world and is willing to hive some of their resources to help.
[Link of the second prospect user](https://drive.google.com/file/d/1KZsWrp6Pnw4rmzYOWMH1B8FonbQKXoFX/view?usp=sharing)  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteerProspectUser2.png)

For the third prospect user we focused on an user that is attending and reviewing all the applications of an organization. This user wants many people to apply but also wants that the selection process is done efficiently.
[Link of the third prospect user](https://drive.google.com/file/d/1lWLRxb3fqAsmPdrMpC79esXM9NHS7qvC/view?usp=sharing)  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteeringAsProspectUser3.png)


The last one, which is the analytics one, we focused on the person that need the most data about the process: The Volunteering organization administrative. This person needs to check the evolution of the process through time and also see the metrics about the quantity of donations. Also, the volunteer admintistrator wants to know the type of people that are aplying to the organization. This kind of persona is the one that needs the most data an insights about the process and the application in general.

[Link of the analytics persona](https://drive.google.com/file/d/1inf0188GK49GLhptSGX-BNvuVQWtdA_6/view?usp=sharing)  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/Deliverable/img/Analytic_persona_Volunteeringg.png)



# Empathy Maps :bookmark_tabs:
### Empathy Map 1: 
For the first empathy map, we made an empathy map focused on the person that we interviewed first: Laura Ávila. She is currently a volunteer in 'Con las manos' organization and has told us about the whole process of looking for a volunteering organization and applying to one. The link for that empathy map can be found [*here*](https://app.mural.co/invitation/mural/moviles6247/1598283643600?sender=u0569ebba38d5a7f68a3f5515&key=a6663901-24c9-4347-8af6-8ef5133cd18b)

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/EM1V.PNG)

### Empathy Map 2: 

For the second empathy map, we focused on an user that asked for donations on the Facebook groups 'Cbus que deberían meter' and 'Cbus sin censura'. We selected that user because we think that donating is an essential part of helping people and has a high correlation with volunteering and social work. The link for this empathy map can be found [*here*](https://app.mural.co/invitation/mural/moviles6247/1598283670782?sender=u0569ebba38d5a7f68a3f5515&key=b97b62d2-7aec-4899-a40d-45a683395321)


![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/EM2V.png)

### Empathy Map 3
For the third empathy map, we focused on the other side of the volunteering process. Inside the organization, there are people responsible for doing the application process and in charge of the activities in the organization. We tried to connect the pain points that happened in one extreme of the process with the other. The link for this empathy map can be found [*here*](https://app.mural.co/invitation/mural/moviles6247/1598283694485?sender=u0569ebba38d5a7f68a3f5515&key=bca8a455-14c9-44a5-8a6f-414bf58e0f73)

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS3/imgs/EM3V.png)
# VD Maps :pushpin:
This is the VD Map developed, based on each one of the 41 questions. The VD Map can also be found on the following [**link**](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteeringVDMap.png) The classification can be found on the Google Docs in the Business questions section:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VolunteeringVDMap.png)

In the following map, we focused in displaying all the business questions and relate all the data we consider is relevant for obtaining insights from the data.

# Business questions :telescope:
For the questions, we decided to write them in order (From type 1 to type*). We wrote them on a Google Docs to classify them by colour based on their type and also did a classification for the VD Map based on the datasets each question was relating to. This document can be accessed [**here**](https://docs.google.com/document/d/1RhlMFhXXHXKvlGbRF1j5PKyobKw_EH49C-7jzGjUxRI/edit?usp=sharing). The questions are listed below (There's 1 extra question):

1. What is the Android distribution of our client devices? What is the percentage of users that use our platform on an iOS device?
2. What is the percentage of users that have the latest Android and ios operating systems?
3. Does the app load in less than 4 seconds ?
4. Is the app fluent in all its screens (less than 0.2 ms between transitions)?
5. Are the last release bugs lower than our goal (max. 3 bugs) ?
6. How many days passed since a volunteering org. was visited by a volunteer from the app? → Recommend to the users if there are a lot of days.
7. From the volunteering organizations that are presented as recommended to the user, what is the percentage of organizations that the user really explores? 
8. From the nearby volunteering opportunities that appear to the user, what is the proportion of volunteering organizations that the user really explores? 
9. What is the average time that an organization expends on accepting a volunteer? → If the time is greater than X, we should talk with the organization.
10. From the times the user opens the application, in what percentage of times does the user check his donation’s history?
11. What is the average frequency that the user changes his profile information?
12. How many days have passed since a user applied to a volunteering organization? 
13. On average, how much time does the user take to level up? -> We could adjust the amount of xp  gained by volunteering or by donating
14. Which is the percentage of users that redeem each kind of prize?
15. What is the distribution of users that end the volunteering application at early steps of the process? → We could identify blocking steps.
16. For the organizations, what is the best month to launch a donation campaign? In which month do they generate more income?
17. What is the percentage of users that haven’t used our app in the last 15 days?
18. What is the percentage of users which enable their geolocation? 
19. What is the percentage of users that play announcements to donate?  We could know if it’s relevant to this functionality.
20. What is the percentage of users that actually redeem their prizes gained when they level up? 
21. What is the percentage of users that apply to a volunteering opportunity but haven’t made any donations?
22. What is the heatmap of the organizations where users are going?
23. On average, how much time a user spends on an organization?
24. What are the user’s most frequent locations? Does the average user have a regular path?
25. Which are the geographical points where users are more frequent to apply to a volunteering organization?
26. What are the user’s favourite categories on their recommendations ? 
27. On average, which are the hours that a volunteer has validated through our app?
28. What is the progress, on average, in each one of the stages on the overall applications to a volunteering opportunity? How can we present those results to the volunteering admin?
29. On average, at what percentage of completion does the donation process is when x days have passed? 
30. What is the amount of users that contribute with donations after being volunteers?
31. What is the percentage of high-impacting volunteering organizations that the users do not apply to them? 
32. What is the screen where the users stay the most?Ads
33. What is the percentage of acquisition of users from each channel where they downloaded the app?
34. How much income from donations gets a volunteering organization per month? we can use this to get to more organizations.
35. What is the percentage of volunteering organizations that registered on the platform in the first year since our launch?
36. What is the amount of organizations that are connected to Unicef, United Nations or other globally recognized NGO, which are prefered by the users?
37. Which is the location distribution of the organizations? → Recognize new points to add organizations.
38. What is the percentage of users that have the last version of the app?
39. What is the proportion of users that donate physical materials over money? 
40. In which amount users uninstall the app after being volunteers for at least once ?
41. Does the user interact with the dashboard about his information(volunteering hours/month, aptitudes acquired on each volunteering org., etc)? In what percentage? → See if they do it so we can add goals that the user can set up to himself

# Functional scenarios :beginner:
The funtional scenarios we decided to build, are displayed below. These functional scenarios explain the functionalities that we consider that our app must have with the users path inside them.

## See the available prizes
| Scenario ID | F-SCE-01 |
|-|-|
| Scenario Name | See the available prizes. |
| Description | As a user I want to see the prizes with the description of the requested tasks to win it. Also, I only want to see the available ones.  *Path:* I go to the prizes lists and then I only see the available prizes. |
| Priority | 3 |
| Complexity | 2 |

## Redeem a prize
| Scenario ID | F-SCE-02 |
|-|-|
| Scenario Name | Redeem a prize. |
| Description | As a user I want to redeem a prize when I have enough points. Also, I need to see the prize at the list of my previous prizes so that I can see what I have redeemed. *Path:* I select the desired prize and redeem it. Also, my points are discounted and that prize information is moved to the history. |
| Priority | 3 |
| Complexity | 2 |

## List the prizes I redeemed
| Scenario ID | F-SCE-03 |
|-|-|
| Scenario Name | List the prizes I redeemed. |
| Description | As a user I want to see all the redeemed prizes with its respective date. *Path:* I go to my profile and click at the prizes section, i have to see all the redeemed prizes. |
| Priority | 2 |
| Complexity | 2 |

## See the points associated with each charity organization
| Scenario ID | F-SCE-04 |
|-|-|
| Scenario Name | See the points associated with each charity organization. |
| Description | As a user I want to see the points associated with each charity organization and its respective activities. *Path:* When i see the list of organizations i want to see the detailed information and get informed about the possible points to gain at the different activities. |
| Priority | 3 |
| Complexity | 1 |

## See current points
| Scenario ID | F-SCE-05 |
|-|-|
| Scenario Name | See current points. |
| Description | As a user I want to see my current points in order to know what prizes I can apply for. *Path:* When I enter my profile I want to be able to see my available points at the moment. |
| Priority | 3 |
| Complexity | 1 |

## Recommendation on possible donations that organizations request through the app
| Scenario ID | F-SCE-06 |
|-|-|
| Scenario Name | Recommendation on possible donations that organizations request through the app |
| Description | As a user, I want to find recommended donation requests based on my interests  or location so I can choose the best type of donation for me. *Path:* In this scenario, the recommendation system recommends donation offers to me based on my interests or my location and then I choose the one he prefers the most. |
| Priority | 3 |
| Complexity | 5 |

## Search a donation request by name, type of donation or donation organization
| Scenario ID | F-SCE-07 |
|-|-|
| Scenario Name | Search a donation request by name, type of donation or donation organization. |
| Description | As a user, I want to search for a donation offer by name, organization or type of donation in the application so that I can find one that I want to choose to donate. *Path:* This scenario focuses on my searching for a donation. I first type the words on the searchbar and then the donations appear being filtered based on their matching with the type, organization or name of the donation offer. |
| Priority | 1 |
| Complexity | 3 |

## Post of a donation request/offer
| Scenario ID | F-SCE-08 |
|-|-|
| Scenario Name | Post of a donation request/offer |
| Description | As an organization, I want to be able to post a donation offer/request on the application so that people that want to donate can find my post and donate. *Path:* This scenario describes that first, I must fill the characteristics of the donation (type of donation, name, value, etc.) to post the donation offer. Then I post it and wait for people to donate.  |
| Priority | 5 |
| Complexity | 3 |

## See donation progress
| Scenario ID | F-SCE-09 |
|-|-|
| Scenario Name | See donation progress |
| Description | As a user, given that I already donated, I want to be able to see the impact of a donation I make so that I can trust the organization that I donated and keep donating. *Path:* I am going to see the donation and the posts the organization is going to make to show the impact of the donation and the truthness of it to everyone that donated. This impact is also going to be shown to me with metrics related to the amount collected and the people reached and helped. |
| Priority | 2 |
| Complexity | 4 |

## Donate on a donation offer
| Scenario ID | F-SCE-10 |
|-|-|
| Scenario Name | Donate on a donation offer |
| Description | As a user, given that I selected a donation offert, I want to donate to it, so that I can be generous and contribute to the organization I chose and its social causes. *Path:* In this scenario I select a donation offer that I find, and then proceed to donate. Depending on the type of donation, I am going to be able to see the banking accounts where I want to donate, or the system contact the organization through a push notification or message so that the organization calls me to establish the picking of the goods to donate. |
| Priority | 5 |
| Complexity | 5 |

## Create a recruitment announcement
| Scenario ID | F-SCE-11 |
|-|-|
| Scenario Name | Create a recruitment announcement |
| Description | In case my organization needs volunteers for their cause, I can send a recruitment post in the application to all interested volunteers that want to apply. *Path:* When logged in as an organization leader, I can go to the section where I can create a recruitment and fill the information desired for the volunteering job to be created, so that people can apply to it. |
| Priority | 5 |
| Complexity | 4 |

## List all candidates
| Scenario ID | F-SCE-12 |
|-|-|
| Scenario Name | List all candidates |
| Description | When a recruitment is launched, I must be able to get a list of all the users that applied to the offer in the app seeking to volunteer for their cause.  *Path:* When logged in as an organization leader, you can go to the recruitment section and select the one you want to check into and it will display the people that applied for the offer. |
| Priority | 4 |
| Complexity | 3 |

## Get the detailed information of each candidate
| Scenario ID | F-SCE-13 |
|-|-|
| Scenario Name | Get the detailed information of each candidate |
| Description | In the list of candidates from a particular recruitment, I should be able to access all the information requested in the form and that was filled by each candidate.  *Path:* When seeing all the candidates up to that moment that applied to an offer, I can select a candidate and it should show the details from that specific person (relevant information, background, etc). |
| Priority | 3 |
| Complexity | 4 |

## Finish a recruitment offer
| Scenario ID | F-SCE-14 |
|-|-|
| Scenario Name | Finish a recruitment offer |
| Description | As an organization, I should be able to finish a recruitment offer manually, avoiding more people to apply. This process is also automatic, when the due date of an offer is on or after the current date. *Path:* Inside one recruitment offer, the organization can finish the recruiting so that no more people can apply to it. The automatic process just closes the offer when the due date has passed. |
| Priority | 4 |
| Complexity | 2 |

## Accept a candidate from a recruitment offer
| Scenario ID | F-SCE-15 |
|-|-|
| Scenario Name | Accept a candidate from a recruitment offer |
| Description | Given that I see one candidate that applied, as an organization, I should be able to accept a candidate that applied for the recruitment offer.  *Path:* Inside the profile of a particular candidate that applied to a recruitment offer, I should be able to approve him, notifying the candidate of the acceptance for him/her to be a volunteer in the organization. |
| Priority | 4 |
| Complexity | 3 |

## Browse through the Volunteering Opportunities
| Scenario ID | F-SCE-16 |
|-|-|
| Scenario Name | Browse through the Volunteering Opportunities |
| Description | I want to find recommended organizations so that I can select the ones that fit me the most. *Path:*  I want to go through the different recommendations that the app is showing to me based on my profile preferences and location. Also, If I want I can search for a specific organization. I also want to filter by type. I want to check a possible organization to find detailed information about it. Finally, the app should only recommend me organizations that are accepting volunteers at this moment. |
| Priority | 5 |
| Complexity | 4 |

## Apply to a Volunteering Organization
| Scenario ID | F-SCE-17 |
|-|-|
| Scenario Name | Apply to a Volunteering Organization |
| Description | Given a particular volunteering organization, I want to apply immediately so that I can get accepted faster.  *Path:* I want the app to automatically set the information that I have previously given in my profile. After that, I want to fill the remaining questions and upload the documents that are requested. Also, I want to schedule the date for the application interview and add that as a reminder on my calendar. |
| Priority | 5 |
| Complexity | 3 |

## Check the Application Status
| Scenario ID | F-SCE-18 |
|-|-|
| Scenario Name | Check the Application Status |
| Description | Given that I have applied to a volunteering organization, I want to check the status of the admission process so that I can keep track of the global process.  *Path:* I want to see the progress of my request in real time. Also, I want to see approximate dates for the next steps so that I know how much to wait. |
| Priority | 4 |
| Complexity | 2 |

## Confirm the Acceptance or Rejection
| Scenario ID | F-SCE-19 |
|-|-|
| Scenario Name | Confirm the Acceptance or Rejection |
| Description | Given that I have applied to a volunteering organization and the process has come to an end, I want the application to notify me of the results of the process so that I can decide what to do next.  *Path:* In case I get accepted, I want the application to tell me the next thing to do and to tell my confirmation to the organization. In case I get rejected, I want to archive that process and be able to apply again if possible. |
| Priority | 3 |
| Complexity | 1 |

## Schedule the Volunteering hours
| Scenario ID | F-SCE-20 |
|-|-|
| Scenario Name | Schedule the Volunteering hours |
| Description | Given that I have been admitted to an organization, I want the application to automatically schedule my volunteering hours and remind me of general notifications about my work. Also, I want to validate the volunteering hours I do in the organization so that I can get a certificate and visualize how much I have helped.  |
| Priority | 2 |
| Complexity | 3 |

# Quality scenarios :diamond_shape_with_a_dot_inside:
Here we present the quality scenarios we built for the application.

## Lack of internet connection during a donation transaction
| Scenario ID | Q-SCE-01 |
|-|-|
| Scenario Name | Lack of internet connection during a donation transaction. |
| Quality Attributes | Eventual connectivity, resilience |
| App Status and Context | The mobile app is requesting data for executing a transaction. |
| Changes in the context | The user loses his connectivity, then the data connection is lost. |
| System Reaction | The app notifies the user about the poor connection and the transaction is moved for resuming later. Nothing is registered in the Database because a rollback is made.  |

## Unavailability of GPS Connection
| Scenario ID | Q-SCE-02 |
|-|-|
| Scenario Name | Unavailability of GPS Connection |
| Quality Attributes | Eventual connectivity |
| App Status and Context | The app requests for the user geolocation using the corresponding API. |
| Changes in the context | The API answers with the user’s last location in the best case. In the worst case the API answers with no location. |
| System Reaction | The system (backend and frontend) uses the user’s last location to show recommendations in the app based on it (Informing the user that those recommendations were given using his/her last location). If no location is given, the app shows recommendations based on the last stored/cached location of the user. If there’s no cached nor last location retrieved, the app shows a default recommendation list and informs the user that they are not recommended using his location. |

## Functionality During Low Battery Mode
| Scenario ID | Q-SCE-03 |
|-|-|
| Scenario Name | Functionality During Low Battery Mode |
| Quality Attributes | Performance |
| App Status and Context | The user’s device is getting low on battery, so the user decides to activate the low battery saving mode. This mode disables background data usage, and delays some notifications.  |
| Changes in the context | The energy of the cell phone begins to get lower and the resources available by the OS are limited. |
| System Reaction | Some results and recommendations aren’t as precise and the actualizations are less frequent. The background processes stop and the state of the app is saved. |

## Memory Pressure on the Phone
| Scenario ID | Q-SCE-04 |
|-|-|
| Scenario Name | Memory Pressure on the Phone |
| Quality Attributes | Performance, Resilience |
| App Status and Context | The app is showing the feed of recommended volunteering organizations, the user has other apps open that are also consuming memory. Finally the user puts the volunteering app in the background. |
| Changes in the context | The Viking Killer in the Android Os begins to kill the processes. |
| System Reaction | To avoid issues when moving the app from background to foreground, the state is saved. |

## Dark mode suggestion on low luminosity
| Scenario ID | Q-SCE-05 |
|-|-|
| Scenario Name | Dark mode suggestion on low luminosity |
| Quality Attributes | Accessibility, User experience |
| App Status and Context | The mobile app is running and the user is using it in an environment of normal or high luminosity. |
| Changes in the context | The user enters into a dark zone and he wants to see the app adapted to a darker zone so his eyes are not affected. |
| System Reaction | If the cell phone has a luminosity sensor, the app shows a message to the user, suggesting him to change to a dark mode of the app in the configuration. After this, the app can still be used with facility. |

## Problems loading the screens because of overdrawing, resizing, main thread overloaded
| Scenario ID | Q-SCE-06 |
|-|-|
| Scenario Name | Problems loading the screens because of overdrawing, resizing, main thread overloaded |
| Quality Attributes | Latency, Performance |
| App Status and Context | The mobile app is requesting the mobile organization's information but the screen is white. |
| Changes in the context | There is a problem with a front end feature and the app is not answering. |
| System Reaction | The app should immediately put a loading screen using another thread different from the main one, because that one may be busy and can’t render correctly the graphical components in the screen. After some time, tell the user that the feature is unavailable if it wasn’t able to work and give the option to return to the last functional screen. |

## Managing of transactions in volunteering offers applications
| Scenario ID | Q-SCE-07 |
|-|-|
| Scenario Name | Managing of transactions in volunteering offers applications |
| Quality Attributes | Concurrency |
| App Status and Context | Different users apply to the same volunteering offer in a short period of time, sending requests to the database of the application. |
| Changes in the context | The application’s database receives all the requests corresponding to the user’s applications to the same volunteering offer and tries to store them. |
| System Reaction | The system is going to work with transactions, so even though the requests were made almost simultaneously, the database will accept them in order. If the capacity of maximum applicants is reached on an offer, the application will show a message of error to the user informing that the places were filled up, making a rollback for them because their transactions couldn’t be completed. |

## Unavailability to access the Database
| Scenario ID | Q-SCE-08 |
|-|-|
| Scenario Name | Unavailability to access the Database |
| Quality Attributes | Availability |
| App Status and Context | The database service stopped working for networking issues from the cloud provider, and it can’t be reached by the app at a certain moment. The app is in regular conditions and can execute any request at any time. |
| Changes in the context | The app will try to access the database through a service of the API and it will return an error, given that at the moment of connecting to the database it didn’t answer as expected. |
| System Reaction | The app, in the best of cases, shows cached information (such as old posts, old search results, etc.). In the worst case, it shows an error message showing that the service is unavailable at the moment. |

## Unavailability to connect to an external API
| Scenario ID | Q-SCE-09 |
|-|-|
| Scenario Name | Unavailability to connect to an external API |
| Quality Attributes | Availability |
| App Status and Context | The volunteering app is trying to connect to an external API, such as the google maps API, but the service is not responding for them.  |
| Changes in the context | A 50X error was emitted. The service is unavailable for some reason. |
| System Reaction | The app should notify the user that the request wasn’t possible and ask to try again. |

## Possibility of a Data Leak
| Scenario ID | Q-SCE-10 |
|-|-|
| Scenario Name | Possibility of a Data Leak |
| Quality Attributes | Security |
| App Status and Context | The user registers on the application or logs in to use the application. However, another application is trying to access his personal information. |
| Changes in the context | The user’s information is stored internally on the phone. |
| System Reaction | To secure the user’s information, this internal storage of his sensitive/personal data is encrypted so that other applications are not able to read and process it. The malign software is not granted access to the system data and in the case the foreign app accesses the data, it won’t be able to interpret or use it due to the encryption. |

## Performance during a heavy loading list
| Scenario ID | Q-SCE-11 |
|-|-|
| Scenario Name | Performance during a heavy loading list |
| Quality Attributes | Performance, GUI responsiveness |
| App Status and Context | The app requests a list of elements (donations, organizations, etc) and it is retrieved. |
| Changes in the context | The list of elements that was requested by the user quen entering a screen is rendered on the screen and the user starts to scroll through it. |
| System Reaction | To avoid the delay and the poor performance of the application, if the list is too long, the application will perform as an infinite scroll list. This means that the elements that the user is not seeing in the screen are not pre-rendered. Those elements render while the user is near to them so that the performance of the app is not affected. This behavior refers in the backend to pagination. |

## Unavailability to Save/Submit
| Scenario ID | Q-SCE-12 |
|-|-|
| Scenario Name | Unavailability to Save/Submit |
| Quality Attributes | Availability, resilience |
| App Status and Context | The user is filling a form to apply to an organization. Unfortunately, the user is disconnected from the internet and when the user clicks submit, the information can’t be sent. |
| Changes in the context | There is poor internet connectivity so the user exits the form before submitting. |
| System Reaction | The app should save the information that was filled by the user so that when the user is trying again to submit, there is no need to fill the form again. |

## High amount of parallel users applying to an organization
| Scenario ID | Q-SCE-13 |
|-|-|
| Scenario Name | High amount of parallel users applying to an organization. |
| Quality Attributes | Scalability, availability |
| App Status and Context | Multiple users proceed to apply at the app. The mobile app sends simultaneous requests to the backend service. |
| Changes in the context | The amount of requests that the backend is trying to resolve is increasing. |
| System Reaction | The backend has a multithreading reception system that helps to handle multiple requests at the same time. The user will see a loading icon until the request finishes successfully. If the process fails, the backend will rollback the transaction and the app will show the error to the user. |

## Multiple spamming on the same button
| Scenario ID | Q-SCE-14 |
|-|-|
| Scenario Name | Multiple spamming on the same button |
| Quality Attributes | Performance |
| App Status and Context | The mobile app has an available button at a particular screen. |
| Changes in the context | The user starts to click repeated times over the button. |
| System Reaction | The app blocks the button tapped after the first time while it's requesting/sending information from/to the backend. When it finishes, the button will be unlocked and a message will inform the user if the process was successful or it failed. |

## Change in Font Size
| Scenario ID | Q-SCE-15 |
|-|-|
| Scenario Name | Change in Font Size |
| Quality Attributes | Accessibility |
| App Status and Context | The mobile app is designed with certain dimensions for spacing and fonts. |
| Changes in the context | The user has a default configuration that increases/ decreases the font size. |
| System Reaction | The mobile app adapts and orders again the interface to provide a great user experience. |

# Business model and value proposition :trident:
The business model that we developed for this idea is divided into 3 parts: Value proposition (using the template), Revenue model and Cost model.

## Value proposition
For the value proposition, we followed the temaplate:

### Customer reachability
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VP-Volunteer1.png)

### Key necessity
This is a problem of two tails. The ONGs need to do a lot of work to recruit volunteers. Also, the possible volunteers are people that don't know how to help and donate. There isn't enough updated information and it is spread around the internet and social media.

### Value proposition
Centralize the information about ONGs and simplify the application and donation processes to possible volunteers.
The key words in this approximation are: centralize and simplify, because the users want an easier and effective process.

## Revenue model
The revenue model is based on the following items:

### In-App Advertising
We decided to include advertising on the screen that the users see the most, that we assume at first it is going to be the home of the app, where the recommendations of volunteering organizations are going to appear. This will help us with money that we can spend on maintaining the application. If we have a total of 20.000 users monthly and we earned $0.1 dollars for each one, we would obtain $2.000 dollars.

### Volunteer donations
The users are going to be able to donate to the developer team, the amount of money they consider. This to help the operation and maintenance of the application. As this application focuses on volunteering and donations, we are not expecting to gain a lot of money with it, just the necessary. Assuming 20.000 users monthly, we could obtain an average of $500 dollars if the 2.5% of our users donate 1 dollar each.

### Data monetization
We can sell the data to the United Nations or another organization that’s interested in how volunteers are contributing to social problems in Colombia, and even in the world if our platform is used outside the country. This will also help the operation and maintenance of the application. The amount of money earned from this depends on the organization.

## Cost model
The cost model is based on the following items:

### Servers, backend engine, etc.
We will have a cost associated with the operation of the application. This cost refers to the hosting and the servers of the backend of our application, which is going to be in the cloud (with fault-tolerance and high scalability to support a large number of users). We will also have costs to publish the app on App Store and Google Play Store, and on maintaining our analytics backend and services.

Assuming that the backend is completely in Firebase:  
With 20.000 users, if we assume that the users make 30 writes to the DB in average, and read from the DB 1750 times per month, and we have and amount of data equivalent to 18GiB, and we store each one of the files and photos and have 40GB of storage in the DB, and users call the serverless part of our backend 500 times per month each one of them, we will get a total monthly cost of $104 dollars (including other costs such as Egress GB transmitted cost, TestLab, etc).

### Salaries
As a team of 4 members, we estimate that each one of us is going to earn the minimum legal salary in Colombia ($265,52 dollars). This gives us a total of $1062.08 dollars to be paid each month.