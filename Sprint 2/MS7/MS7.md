# Gitflow :repeat:
In this section, we want to describe some decisions that are relevant to handle the git-flow methodology and the course guidelines.

1. We use protected branches (_dev_ and _master_) for each project. This decision enables us to generate sprint releases at the _master_ (production) branch and develop with a continuous review (by the merge requests) at the _dev_ branch.
2. **Merge requests**:  
2.1 **Approval**: Each MR needs at least one Gitlab approval to be merged.   
2.2 **Labels**: Each MR has to have one label (at least) to identify what is its purpose.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/MR%20labels.png)
2.3 **Squash**: The squash option is set by default.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/Squash%20option.png)

3. **BE Deployment**

For the BE app, we create a deploy pipeline that works at each MR from feat or fix branches to dev and master. The automatic deploys are executed after the code review. This pipeline builds and launches the backend into their associated environment. (Heroku).

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/Gitlab%20pipeline.png)

# Backlog :card_index:
1. We create the issues as HUs to identify what is the specific requirement and how it is related to a stakeholder.
2. We create **labels** for facilitate the differences between each one. For example, a requirement related to the login screen will have two issues: one for Flutter and another for Kotlin.
Also, there are labels for features and fixes. At this point, the board will look something like this.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/Board%20AK.png)

3.  We could relate issues. If there are tickets that are related we could connect it to know the possible future dependencies.
4. If we detect that an issue is big, we could associate it as an EPIC and generate its children.
5. To follow specific criteria of acceptance the issues can have a checklist.
# Design patterns :man:
[NOTE: More information about the design patterns used can be found in this document](https://docs.google.com/document/d/17v-WemMDVKMnm7lGJUYpZLeZEmjbntNUVTD8slVaL8U/edit?usp=sharing)
## **Flutter App**
### **BLoC pattern**
The business logic component is based on Reactive programming to handle the flow of data within an app. This pattern is based on _streams_ of events, handles the business logic, and publishes streams of data to the widgets.

At this point, we implement a LoginBloc. This bloc also has a LoginProvider that is an Inherited Widget. We could see this implementation at the following snap code:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/bloc%20code.png)

## **Kotlin App**
## MVVM
We implement MVVM as the design pattern that will allow us to organize and separate the responsibilities of the application. First, we have the Models that define the logical interfaces of the data received from the backend or external APIs. Secondly, the ViewModels (represented in the UI folder), it works as an intermediary actor and handles the logic and behavior of the view. Third, the views are described by the .kt which serve as intermediaries with the XMLs.

## Singleton

For managing the repositories files (which works as an intermediary layer between the ViewModel and the models) we used a Singleton that enables us to have one reference for each view model that consumes the data. This is useful to have a common reference and avoid misunderstandings.

## Factory pattern

This pattern allows you to generically handle the use of viewmodels in your application. In this case, since there are multiple instances of LoginViewModel, they are all delegated by this generic mediator class called LoginViewModelFactory. This with the purpose that, in case screens are changed or there is a change in the layout or state of the phone (such as rotating it), the information is persisted in said viewmodels. But since there can be several instances, it is not ideal to handle each instance manually, a factory is handled that delegates all those that exist generically.

## **Backend**
### **DAO**
We use this pattern to encapsulate data access.  The business layer is worried about the logic and uses the DAO for the interaction with the data sources.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/DAO.png)

Now, we need to take into account the quality scenarios that we made for the past sprint, and based on those, determine the best design pattern. 

[In this document you can find the quality scenarios that we made for ventures. ](https://docs.google.com/document/d/1kxAl2PYekeQ3gJOHb8liyDCRKkTJcKdVU2TjYqdrNCs/edit?usp=sharing)

### Justification of the primary Quality scenarios

* Q-SCE-01 Lack of internet connection during a Payment processing: For this QS, the design pattern that we plan to use to ensure the quality of the application during that extraordinary event, we are going to use the DAO. this way, the product ordered by the user is retrieved from the database and the payment process is handled by the user's device until the payment is validated.

* Q-SCE-04 RAM Memory Pressure on the Phone: For this QS, we found that data binding the current state of the app is a good way to keep the information of what was happening in the application before it was put in the background. With this, we can save the steps in the different types of transactions with ease.

* Q-SCE-07 Unavailability to access the Database: Since we are working with a facade, this problem of not being able to access the DB can be solved by utilizing the data that passed through the facade most recently. This way, the user can still use the application with the data that was on the cache.

* Q-SCE-10 Possibility of a Data Leak: We are using JWT to authorize our users with great security and also to maintain secure the business layer.



# Architectural design :part_alternation_mark:

## **UML**

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/UML%20Creatur.png)

This diagram enables us to explain some relevant decisions that we take to maintain efficient business logic and to accomplish all the requirements.

1. A user can enter the app and access to all the services. However, the most important thing is that a venture is also a user. This means that a user with a venture profile could buy products and services from other ventures.

2. Our ventures could have products and the users could make orders with multiple products.

3. An order has a review. This enables a user to grade the service.

[Link to UML](https://drive.google.com/file/d/1sPOnTeXJsIC-EY54xdh-sjwTnW1mtGdo/view?usp=sharing)

## **Components diagram**

This diagram describes the organization and wiring of the components in our system. Then, this system has two subsystems that represent the business requirements processing, the first one, the analytics that is related to the process that works the data to bring new functionalities. In the second one, the backend is used for handling business logic and connection with the databases.

Besides, this subsystem has components that describe the relevant logic pieces of the server. At the backend, we have the VentureSystem, ProductSystem, OrderSystem, and Payment, and for the analytics, there is the recommendation system. Also, we want to describe how these systems are connected with external APIs and Databases. On the other hand, we represent two mobile apps because this is the client-side component.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/Creatur%20component%20Diagram.png)

[Link to Components diagram](https://drive.google.com/file/d/1a96tn1I0whrEQkK-hRqTQ4XdfXKmFaxO/view?usp=sharing)

## **Deployment diagram**

At this point, we want to represent the artifacts that we used to deploy the components. We take several decisions that can be seen in this diagram.

1. Heroku: We decide that Heroku is a nice idea to deploy our backend and analytics service because it is free and provides easier tracking of errors and issues than their competitors.
Also, at Heroku, we decide to use PostgreSQL as our DB. The database decision was really difficult because there were many options (Firestore, MongoDB, RedisDB, etc) but we choose Postgres because it's a relational database that can create meaningful information by joining tables. At this point, we will have the ability to make SQL basic operations and this is powerful to our business because we usually want to generate reports, execute transactions, and handle a deep logic.
2. Firebase storage: We have to save photos from the venture's products. The Postgres DB cannot save this type of information so to do that Firebase helps us. We will use this as an S3 Bucket.
3. Google analytics API: This API will help us to track and manage the principal statistics over the app and the user's behavior.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/Creatur%20eployment%20Diagram.png)

[Link to deployment diagram](https://drive.google.com/file/d/1-P3UAG8bKKSd9Vqbhj9fzmcEqtk3YlWz/view?usp=sharing)