# Gitflow :repeat:
In this section, we want to describe some decisions that are relevant to handle the git-flow methodology and the course guidelines.

1. We use protected branches (_dev_ and _master_) for each project. This decision enables us to generate sprint releases at the _master_ (production) branch and develop with a continuous review (by the merge requests) at the _dev_ branch.
2. **Merge requests**:  
2.1 **Approval**: Each MR needs at least one Gitlab approval to be merged.   
2.2 **Labels**: Each MR has to have one label (at least) to identify what is its purpose.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/MR%20labels.png)
2.3 **Squash**: The squash option is set by default.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/Squash%20option.png)

3. **Backend Deployment**

For the Backend app, we create a deploy pipeline that works at each MR from feat or fix branches to dev and master. The automatic deploys are executed after the code review. This pipeline builds and launches the backend into their associated environment. (Heroku).

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/MS7/img/Gitlab%20pipeline.png)

# Analytic questions

We decided to create 10 Analytic Questions. These questions take advantage of the data registered by the app to generate valuable insights. Moreover, some Analytics Questions are type 2 questions and can be consumed by users during their interaction with the app. Other questions are focused on generating insights about the usage of the app and the demographics of the users and the ventures. 

1. Where are the ventures located and how many products is each venture offering?

2. How many products/services is each vendor offering on average per category?
3. Do we have some information about user preferred product categories? Which are those?
4. Do we have information about the proportion (by age) of users that buy products in the app?

5. Do we have information about the proportion (by sector) of users that buy products in the app?
6. Do we have information about the age of the users in order to classify and relate their favorite products?

7. Do we have information about the location of the users that frequently buy products through the app in comparison with the most popular ventures? 

8. What is the price distribution of the products per category?

9. In which sectors do the ventures have higher prices?
10. What is the category distribution in our app for all of the ventures?

We hope that, with these questions, we can get valuable knowledge about how the app is being used.


# Analytics Stack Design

In order to visualize the questions correctly, first we needed to define and develop an analytics stack pipeline. For the Analytics Stack Design we used the following pipeline:
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/_Diagrama_de_flujo.png)

This pipeline has all the steps that are needed in order to process the raw data and visualize it nicely. The main components we used int this pipeline were mockaroo for the basic database information, postgres for the relational storage and Tableau for the data Visualization.

For the storage step of the pipeline, we used the postgres SQL relational database. We migrated from version 12 of postgress to version 11 so that we could connect to Tableau, since Tableau does not support postgres v12 yet. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/PostgressData.png)

In Tableau, we can connect to the Database and access the data in real time. Also, all the database changes are automatically updated. Because of this, we can say that this type of pipeline is a Real-time/Stream pipeline. The data that was loaded after connecting to the DB is the following:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/imported_tables.png)

For the processing step of the pipeline, we used the tools in Tableau to create unions between the different tables so that we could be able to visualize them together. Also, we created some additional calculated columns to make some data clearer. For example, we converted some ids into ints so that we could create the joins. Also, we calculated the user's age from his date of birth and grouped them into bins. 

The final relationships tree can be seen here:
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/DataUnion.png)

Finally, for the visualization step, we creted different charts and maps that can be used to anwer the analytic questions presented in the above section. The Analytics platform final visualization presentation can be accesed [here:](https://drive.google.com/file/d/10O00cWX3xewx7ZjefTvJMhk_E6h6RMnC/view?usp=sharing)

The raw Tableau file can be obtained [here:](https://drive.google.com/drive/folders/16q_gMkdfwd32K4JBKQmkRv1qNoNGpIv-?usp=sharing)


![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/tableau.png)
# UI/UX Explanation
For UI/UX part, we are going to explain our decisions in terms of the color palette and the fonts we decided to use in our app.
### Color palette
For the color palette, we used the following:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/color%20palette.png)

We decided to use the dark blue as primary color, 2 accent colors (light blue and green) and two contrast colors (the very ligh blue and also the white one). We thought that those colors could transmit a nice sensation in our UI and were very useful to handle contrast in the different screens.

### Fonts
The fonts used in the application are the following:
 1. Rubik ([link](https://fonts.google.com/specimen/Rubik)):  
 This font is used for the logo and for the body texts such as the texts in descriptions. This font is also in the nav buttons labels.
 2. Poppins ([link](https://fonts.google.com/specimen/Poppins)):  
 This font is used mainly for titles.

 ### Logo
 The logo that we currently selected for the application is the following:

 ![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/app%20logo.png)

 We choose this logo because we think that the dinosaur represents the ferocity and greatness that ventures want to have and we also think that the logo is very clean and neat for the purpose of the app and the UI we have.

# App prototype
The app prototype that we developed was done in figma. It has a variety of views that are he following:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/prototype.png)

For accessing the complete prototype to evidence our management of views and the UI/UX, please click the following [link](https://www.figma.com/proto/GqQDnhtI1lfbhJWCXiZEOY/Creatur-Prototype?node-id=4%3A32&scaling=scale-down)

For this prototype, Mario suggested us to change the first prototype we did. This changes were in terms of the register and login views, because our green color wasn't the best for the background color in those two screens.

Because of that, we changed the design views to be white-backgrounded. Here is an image of what we changed:

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/prototype2.png)


# Architecture design
The architectural explanation changes and design can be found [HERE](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/docs/Design%20and%20Architecture%20decisions.pdf) algong with the proper justification for each component. For this reason, the diagrams that we are going to add are already explained in the previous link.

## Components diagram  
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/Component%20Diagram.jpg)

## Deploymnet diagram  

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/Deployment%20Diagram.jpg)

## Data Model
Based on the structure of our problem, we decided to use the following data model that includes every relevant component of our solution.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/DataModel.jpeg)

# Deliverable Definition
The deliverable definition can be summed up in the following table:

| Functionality | Implementation |
|-|-|
| 1	functionality	that	makes	 use	of	one	sensor	in	your	 phone.          | Recommend ventures by the user's location |
| 1	functionality	that	answers	 at	least	one	type	2	question.         | Browse the venture catalog (While the user scrolls the catalog he can see the products and ventures in each category: Recommended and near to their location). The type 2 question is:  Which are the ventures near to my location? |
| 1	functionality	that	is	 context	aware.     | Recommend products by the user's location. The difference of this functionality is that the products are an easy way to show the user the ones near to him, easing the possibility of him to buy something near. the retrieval and treatment of products is different from the one of near ventures. |
| 1	functionality	that	is	 considered	a	smart	feature.       | When the user registers into the application, the app asks some questions to profill the user and then recommend products and ventures based on his answers. We assigned weights of preferences to each one of the products and ventures categories, so that the recommendations correspond to the answers of the user. |
| Authentication | We developed the login and the registration, which are both connected to the backend. |
| One functionality	(different	 form	authentication) should	 be	connected	to	the	backend.        | All the other functionalities are connected to the backend, including login and registration. |

To summarize the explanation of the functionalities, we developed:

 - Recommendation of ventures based on the user's preferences (collected as question answers when the user registers into the app) connected to the backend.
 - Recommendation of products based on the user's preferences (using the same question answers) connected to the backend.
 - Authentication into the app, in terms of login and registration with token retrieval to be used with the other functionalities. This, to be able to communicate to the backend being authenticated.
 - Recommendation of ventures based on the user's location (connected to the backend which processes the user's location for this)
 - Recommendation of products based on the user's location (also connected to the backend as the one before, but trated apart from it to obtain easily the products that the users can be interested on because are near to them).

## Flutter images

### Login

At this screen , the user could enter to the application if he has already an account.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/LoginView.jpeg)
### Signup
At this screen , the user could signup to the application if he has not have an account.
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/RegisterView.jpeg)
### Profiling

At this screen, the user answer to some easy questions to run an algorithm for profiling.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/ProfilingView.jpeg)

### Home

_Suggested_

This view shows to the user the products and ventures that are suggested based on the profiling.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/suggestedHome.jpeg)  

_Near/Closed to you_

This view shows to the user the products and ventures that are closed to his location.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%202/Deliverable/img/NearHome.jpeg)




### Kotlin images

The images from the kotlin app can be found in [this:](https://docs.google.com/document/d/1owmvkYG3bQnbbq-9gtQr32IXuTXjd5HyIxTbBoER8As/edit?usp=sharing) document