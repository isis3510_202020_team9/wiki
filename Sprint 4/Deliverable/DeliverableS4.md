# Value proposition, business model, and value generation

## Value proposition
For the value proposition, we followed the template:

### Customer reachability
![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%201/MS4/img/VP-ME1.png)

### Key necessity
The main problem around this context is the disorder, poor communication, and the lack of a standalone platform or tool to handle the day-to-day activities of a micro-enterprise.

### Value proposition
Provide this necessary platform that improves communication, organization, advertisement, and efficiency in this daily routine.  
All your organization **centralized** in one **standalone** platform.

## Process

The process of building the application led us to consider and equate the functionality and efficiency with what a user requires.
 
In this way, the construction of the application was based on the construction of a prototype developed in Figma that was validated through surveys with future users. However, the focus of the application is on how to provide a better experience and functionality through the proper use of multithreading, memory management, caching strategies, and micro-optimizations.

## Business questions and value generation

### number of orders in each state by category

This question helps us to understand the distribution and stagnation of the orders at each status. This type of data is useful for the ventures because they can understand what stages are making slower the process and for Creatur as an enterprise for improving the order system and avoiding the stagnation at the funnel.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq1.png)

### Date in which the orders are desired

This question helps for knowing as a histogram how is going a day and what date could be potentially overtasked by multiple deliveries and app enters.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq2.png)

### Numbers of each day

This question helps for knowing as a histogram how is going a day and how many orders are for planning purposes.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq3.png)
### Numbers of orders per category each day

Also, is important to know how these orders are divided per category to identify what categories are proper to have more order at certain days of the week.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq4.png)

### Number of orders per location

This should help the ventures know where are the clients. Also, it is useful to calculate the delivery associated costs and predict how much could be spent on this component.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq5.png)

### Average price earned each day

Also is relevant to know how much money is earned by the day to clarify and understand the impact that the products have over the earnings.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq6.png)

### Histogram of sells per week in each category

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq7.png)

### Orders per age group

It is relevant to group by age the orders to understand the user preferences of the products of a venture. In this way, the ventures could take decisions over how important is a product for their clients.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq8.png)

### Orders from suggested ventures per category

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq9.png)

### Orders from suggested products per category

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq10.png)


### Average earnings per rating in the fast-food category

Also is important to understand how its the behavior over the rating and its relation with earnings at a specific sector like food. The most common relationship is between the most expensive orders and an average grade of 2.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq11.png)

### Percentage of users who are also ventures

It is important to know if there is a balance between the number of ventures and users because as a marketplace we want to maintain equity of supply and demand.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq12.png)

### Percentage orders made in the nighttime

In addition to previous analysis about the orders and their system, it is important to know the stage of a day when an order is executed. In this case, we try to study how many orders are made at the night. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq13.png)

### Percentage of orders that were rejected

Also, we want to know the orders that had been rejected. This is important to know how the ventures are dealing with the rejection of an order and what cases are done.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq14.png)

### Percentage of ventures that don't have social media

The relationship with the clients and customers is basic so we want to include their social networks. However, not every venture uses this system so it is important to know the percentage that doesn't have it to talk with them.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq15.png)

### Best ordered ventures in the clothing category

To order the ventures and recognize the most relevant and trendy ventures it is important to know how much sales have they execute. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq16.png)

### Number of orders per price range in the bakery category

This is relevant because it is important to identify and relate the prices with the corresponding categories. This is important for the clients and ventures also for competence analysis.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq17.png)

### Best clients in the pets category

This is relevant because it is important to identify the best clients and try to decrease the churn rate. The best clients could be prized.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq18.png)

### Rating distribution per category

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq19.png)

### Orders numbers in each rating in the toys category

This is relevant because it is important to identify and relate the rating with the corresponding categories. This is important for the clients and ventures also for competence analysis. Also to know the percentage of orders at each level of satisfaction.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/bq20.png)

### Ventures located and products offering

With this Map view, the user can know the location of the different ventures and find the ones that are near him. Also, The size of the circles represents the number of products each venture offers. Finally, the color represents the venture’s category.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq1.png)
### Amount of products offered per category

With this bar graph, we can see that the bakery and sports categories tend to offer more products in each of their ventures. This is useful for knowing which category gives the max amount of products and evaluating the equity because it is important to have a variety in the application portfolio.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq2.png)

### User preferred product categories

With this bar graph, we can see that the users of our app mostly prefer the fast-food category and are less interested in the clothing and pets category. This is useful for knowing the preferences of the users and to manage promotions and campaigns related to their interests.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq3.png)
### Proportion (by age) of users that buy products

This histogram represents the distribution of users per age. Most of our users are 30 years old. This type of data is relevant for profiling and kyc purposes.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq4.png)
### Proportion (by sector) of users that buy products

This map represents all the users in our app and their preferred categories. We can see that at the bottom there is a cluster of users that are interested in the bakery category. This type of information is helpful to find relations between the orders and the location because is useful for identifying key sectors for developing the mark.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq5.png)
### Favorite products by users age

This bar graph represents the preferred category of each of the different age groups. We can see that people in their 35’s are not interested in the sports category but people in their 40’s are. The relation of ages and categories is important because knowing at each category what is the predilected range of ages.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq6.png)
### Compare popular ventures with user location

On this map, we can see the age distribution of our users in each of the sectors of the city. Many of the users that are between 20 and 25 live nearby. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq7.png)
### Price distribution of the products per category

This bar graph represents the price distribution of the products in each category. We can see that the clothing category has the most products and that the toys category has very few cheap products. The price influences the acquisition power because it is needed to make equal behavior. For a venture is important to compare their prices to the competence.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq8.png)
### Sector with higher prices

In this map, we can see the different Ventures and their average prices. Some of the most expensive Ventures are close together. It is important to know which sector has higher prices because the users tend to buy the products at the "near" section of the app.

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq9.png)
### Category distribution per ventures

Here we can see on a more friendly view the percentage of the Ventures that are in each category. The distribution per category is useful for understanding what type of content is viewing the users and what type of products tend to be at Creatur. 

![](https://gitlab.com/isis3510_202020_team9/wiki/-/raw/master/Sprint%204/Deliverable/img/s2bq10.png)

The whole list of Business Questions and their corresponding types can be found [here](https://docs.google.com/document/d/1sDtarh8QP2wLIOTy6xHRx4LCO1odFMfqbIue-uGcsZ4/edit?usp=sharing)
